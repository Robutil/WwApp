var server_socket, state;
var global_counter;
var ses_debug = false;

function heartbeat() {
    connection_send({
        "id": 1
    });
}

function on_open() {
    if (ses_debug) {
        username = "" + Math.random();
    } else {
        var username = prompt("Please enter your username");
    }

    connection_register_username(username);
}

function controller(packet) {
    switch (packet.id) {
        case 1:
            console.log("Answered ping");
            break;

        case 3:
            handle_packet_sys_player_info(packet.options);
            break;

        case PACKET_GENERIC:
            handle_packet_generic(packet);
            break;
        case PACKET_WAIT:
            handle_packet_wait(packet);
            break;
        case PACKET_CARD:
            handle_packet_card(packet);
            break;
        case PACKET_OPTS:
            handle_packet_opts(packet);
            break;
        case PACKET_INFO:
            handle_packet_info(packet);
            break;
        case PACKET_AUDIO:
            handle_packet_audio(packet);
            break;

        default:
            console.log("[Controller]: Unhandled packet: ", packet);
    }
    state = packet.id;
}

function handle_packet_sys_player_info(packet) {
    layout_manager_update_player_status(packet);
}


function handle_packet_generic(packet) {
    console.log("Generic packet: ", packet);
    layout_manager_update_options_werewolf_vote(packet);
}

function handle_packet_wait(packet) {
    var text = "Waiting for players [" + packet.current + "/" + packet.max + "]";

    //Stuff for day-stage
    global_counter = 0;
    if ("skip" in packet) {
        var tmp = window.setInterval(function () {
            global_counter++;
            if (global_counter >= packet.max) {
                clearInterval(tmp);

                var req_audio_msg = {
                    "id": PACKET_SYS_REQUEST_AUDIO,
                    "phrase": "audio/sample.mp3"
                };

                connection_send(req_audio_msg);

                var btn = layout_manager_find_element("skip", undefined);
                if (btn !== undefined) btn.innerHTML = "Ready";
            }
            layout_manager_update_overlay("[" + global_counter + " / " + packet.max + "]");
        }, 1000);
    }

    if (state !== PACKET_WAIT) {
        layout_manager_show_overlay(packet, function (mouse_event) {
            //Make sure button is not double-clicked
            mouse_event.target.parentNode.removeChild(mouse_event.target);

            connection_send({
                "id": PACKET_GENERIC
            });
        });
    }

    layout_manager_update_overlay(text);
}

function handle_packet_card(packet) {
    var type = "type" in packet ? packet.type : "";

    layout_manager_show_card(packet, function (mouse_event) {
        //Make sure button is not double-clicked
        mouse_event.target.parentNode.removeChild(mouse_event.target);

        connection_send({
            "id": PACKET_GENERIC,
            "type": type
        });
    });
}

function handle_packet_opts(packet) {
    var type = "type" in packet ? packet.type : "";

    layout_manager_show_options(packet, function (mouse_event) {
        var clicked_item = mouse_event.target.value;
        var packet = {
            "id": PACKET_GENERIC,
            "answer": clicked_item,
            "type": type
        };
        connection_send(packet);
    });
}

function handle_packet_info(packet) {
    layout_manager_show_info(packet, function (mouse_event) {
        //Make sure button is not double-clicked
        mouse_event.target.parentNode.removeChild(mouse_event.target);

        //Inform server
        var packet = {
            "id": PACKET_GENERIC
        };
        connection_send(packet);
    });
}

function handle_packet_audio(packet) {
    var audio_file_name = packet.phrase;
    audio_play(audio_file_name);
}


document.addEventListener('DOMContentLoaded', function () {
    state = PACKET_USERNAME;
    layout_manager_init();

    if (typeof window["debug_enabled"] === "function") {
        ses_debug = true;
    }

    server_socket = connection_init(on_open, controller);

    //Debugging
    //window.setInterval(heartbeat, 10000);
}, false);

