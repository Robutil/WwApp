var notification_sound = new Audio('audio/sample.mp3');
var audio_buffer = [];

function audio_notification() {
    notification_sound.play();
}

function audio_play(audio_src) {
    var audio = new Audio(audio_src);
    if (audio !== null && audio !== undefined) audio.play();
}

function audio_store_in_buffer(audio_src) {
    audio_buffer.append(audio_src)
}

function audio_play_buffer(item) {
    var audio_file = null;

    if (item !== undefined) {

        if (item === -1) {
            //Play the last item, discard the rest
            audio_file = audio_buffer.pop();
            audio_buffer = [];

        } else {
            //Play the requested item, remove that item
            audio_file = audio_buffer[item];
            audio_buffer.splice(item, 1);
        }
        audio_file.play();

    } else {
        //Play entire buffer, and reset after
        for (var i = 0; i < audio_buffer.length; i++) {
            audio_file = audio_buffer[i];
            audio_file.play();
        }
        audio_buffer = [];
    }
}
