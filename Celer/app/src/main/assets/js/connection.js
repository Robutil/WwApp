const PACKET_GENERIC = 1000;
const PACKET_USERNAME = 1001;
const PACKET_WAIT = 1003;
const PACKET_CARD = 1005;
const PACKET_OPTS = 1007;
const PACKET_INFO = 1009;
const PACKET_AUDIO = 1011;

const PACKET_SYS_PING = 1;
const PACKET_SYS_REQUEST_AUDIO = 2;
const debug = true;

function connection_init(on_open, on_message, on_close) {
    //Connect to server
    var hostname = window.location.hostname;
    var port = "58081";

    var connect_string = "ws://" + hostname + ":" + port + "/";
    var server_socket = new WebSocket(connect_string);

    if (debug) console.log("[Connection](init): Trying to connect to: " + connect_string);

    server_socket.onopen = function (c) {
        if (debug) console.log("[Connection](init): Connection established");
        if (on_open !== undefined) on_open(c);
    };

    server_socket.onmessage = function (msg) {
        if (debug) console.log("[Connection](onmessage): Received unhandled message: ", msg.data);

        if (on_message !== undefined) {
            on_message(JSON.parse(msg.data));
        }
    };

    server_socket.onclose = function (p1) {
        if (debug) console.log("[Connection](onclose): Unhandled: ", p1);

        if (on_close !== undefined) {
            on_close(p1);
        }
    };

    server_socket.onerror = function (err) {
        if (debug) console.log("[Connection](onerror): ", err);
        if (onclose !== undefined) onclose(err);
    };

    return server_socket;
}

function connection_send(data) {
    if (debug) console.log("Sending: ", data);
    server_socket.send(JSON.stringify(data));
}

function connection_register_username(username) {
    connection_send({
        "id": PACKET_USERNAME,
        "msg": username
    });
}