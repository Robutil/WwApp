var wrapper = null;
var werewolf_votes = null;
var layout_manager_player_container = [["1", "2", "3"], ["1", "2", "3"]];

function layout_manager_init() {
    wrapper = document.getElementById("wrapper");

    var btn, menu, close, cross, popUp, option, holder, title;

    btn = document.getElementById('menu');
    menu = document.getElementById('drop-menu');
    close = document.getElementById('close');
    popUp = document.getElementById('pop-up');
    option = document.getElementById('options');
    cross = document.getElementById('cross');
    holder = document.getElementById('holder');
    title = document.getElementById('title');

    btn.addEventListener('click', activate_menu, false);
    close.addEventListener('click', close_menu, false);
    option.addEventListener('click', popUpInfo, false);
    cross.addEventListener('click', popUpInfo, false);

    var achieve_state = "open";

    function activate_menu() {
        if (achieve_state === "open") {
            menu.style.display = 'block';
            btn.style.backgroundImage = 'url(icon/cross.png)';
            achieve_state = "close";

        } else close_menu();
    }

    function close_menu() {
        achieve_state = "open";
        menu.style.display = 'none';
        btn.style.backgroundImage = 'url(icon/menu.png)';
    }

    // function toggleMenu() {
    //     console.log("Toggle", menu.style.display);
    //
    //     if (consumer) {
    //         consumer = false;
    //         return;
    //     } else consumer = true;
    //
    //     if (menu.style.display === 'none') {
    //         menu.style.display = 'block';
    //         btn.style.backgroundImage = 'url(icon/cross.png)';
    //     } else {
    //         menu.style.display = 'none';
    //         btn.style.backgroundImage = 'url(icon/menu.png)';
    //     }
    //}

    function popUpInfo(event) {
        console.log("Clicked");
        holder.innerHTML = null;
        title.innerHTML = null;
        if (popUp.style.display === 'none') {
            close_menu();

            popUp.style.display = 'block';
            switch (event.target.className) {
                case 'player':
                    playerInfo();
                    break;
            }
        } else {
            popUp.style.display = 'none';
        }
    }

    function playerInfo() {
        var table, tr, th, td, player;

        var rows = layout_manager_player_container.length;
        console.log(rows, layout_manager_player_container);

        title.innerText = 'Players';

        tr = document.createElement("tr");

        var th1 = document.createElement("th");
        var th2 = document.createElement("th");
        var th3 = document.createElement("th");

        th1.innerText = "Name";
        th2.innerText = "Status";
        th3.innerText = "Role";

        tr.appendChild(th1);
        tr.appendChild(th2);
        tr.appendChild(th3);

        holder.appendChild(tr);

        th = document.createElement("th");
        for (var j = 0; j < rows; j++) {
            tr = document.createElement("tr");
            for (var i = 0; i < 3; i++) {
                td = document.createElement('td');
                td.innerText = layout_manager_player_container[j][i];

                if (layout_manager_player_container[j][i] === "alive") {
                    td.className = "alive";
                } else td.className = "died";

                tr.appendChild(td);
            }
            holder.appendChild(tr);
        }
    }
}

function layout_manager_update_player_status(player_status) {
    layout_manager_player_container = player_status;
}

/**
 * Resets the entire layout to complete emptiness
 * */
function layout_manager_reset() {
    werewolf_votes = {};

    while (wrapper.firstChild) {
        wrapper.removeChild(wrapper.firstChild);
    }
}

/**
 * Show options plane
 * */
function layout_manager_show_options(container, on_click) {
    layout_manager_reset();

    //Create phrase header
    var phrase = document.createElement("p");
    phrase.innerHTML = container["phrase"];
    wrapper.appendChild(phrase);

    //Create options
    var item;
    var opts = document.createElement("div");

    container["options"].forEach(function (t) {
        item = document.createElement("button");
        item.innerText = t;
        item.value = t;
        item.id = t;

        opts.appendChild(item);
    });

    opts.addEventListener("click", on_click);
    wrapper.appendChild(opts);
}

function layout_manager_update_options_werewolf_vote(packet) {
    var allyTargetPlayer = packet.answer;

    if (packet.name in werewolf_votes) {
        var previous_vote = werewolf_votes[packet.name];
        if (allyTargetPlayer === previous_vote) {
            //Player already voted for this target
            return;
        }

        //Remove old vote
        var prev_element = layout_manager_find_element(previous_vote);
        if (prev_element !== undefined) changeWerewolfVote(prev_element, -1);

    }
    var element = layout_manager_find_element(allyTargetPlayer);

    if (element !== undefined) {
        //Set new vote
        changeWerewolfVote(element, 1);

        werewolf_votes[packet.name] = packet.answer;
    }
}

function changeWerewolfVote(element, changeNumber) {
    var splits = element.innerHTML.split(" ");
    var name = splits[0];

    var new_votes = "[1]";
    if (splits.length > 1) {
        //Increase vote
        var votes = parseInt(splits[1].substr(1, splits[1].length - 2)); //[x]
        console.log(votes);
        new_votes = "[" + (votes + changeNumber) + "]";
    }

    if (new_votes === "[0]") {
        //Remove vote counter, because it is zero
        element.style.color = "white";
        element.innerHTML = name;

    } else {
        //Show vote counter
        element.style.color = "red";
        element.innerHTML = name + " " + new_votes;
    }
}


function layout_manager_show_overlay(packet, on_skip) {
    layout_manager_reset();

    var overlay = document.createElement("div");
    overlay.id = "overlay";
    overlay.classList.add("overlay");

    var animation = document.createElement("img");
    animation.src = "icon/load.gif";
    overlay.appendChild(animation);

    var msg = document.createElement("p");
    msg.innerText = "Waiting for new game this may take several minutes";
    msg.id = "msg";
    msg.classList.add("message");
    overlay.appendChild(msg);

    if (packet !== undefined) {
        if ("skip" in packet) {
            var button = document.createElement("button");
            button.innerText = "Skip";
            button.onclick = on_skip;
            button.id = "skip";
            overlay.appendChild(button);
        }
    }

    wrapper.appendChild(overlay);
}

function layout_manager_update_overlay(text) {
    var msg = layout_manager_find_element("msg");

    if (msg === undefined) {
        console.log("[LayoutManagerUpdateOverlay]: Unable to find message element");
        return false;
    }

    msg.innerText = text;
    return true;
}

function layout_manager_show_card(container, on_click) {
    layout_manager_reset();

    var role_info = null;
    if ("info" in container) {
        role_info = document.createElement("h2");
        role_info.innerHTML = container.info;
        role_info.style.display = "none";
        wrapper.appendChild(role_info);
    }

    var card = document.createElement("div");
    card.classList.add("card");
    card.id = "card";

    var front_image = document.createElement("img");
    front_image.src = "card/cover.jpg";
    front_image.classList.add("card-front");
    card.appendChild(front_image);

    var back_image = document.createElement("img");
    back_image.src = "card/" + container.role + ".jpg";
    back_image.classList.add("card-back");
    back_image.id = "card-back";
    card.appendChild(back_image);

    var begin_button = document.createElement("button");
    begin_button.classList.add("hide");
    begin_button.id = "ready";
    begin_button.innerText = "Begin";
    begin_button.addEventListener("click", on_click);

    wrapper.appendChild(card);
    wrapper.appendChild(begin_button);

    card.addEventListener('click', function () {
        if (card.style.transform === "") {
            card.style.transform = "rotateY(-180deg)";
        } else {
            card.style.transform = "";
        }
        begin_button.className = 'active';
        if (role_info !== null) role_info.style.display = "inherit"
    });
}

function layout_manager_show_info(packet, on_click) {
    layout_manager_reset();

    var info_container = document.createElement("p");
    info_container.innerHTML = packet.phrase;
    wrapper.appendChild(info_container);

    if ("options" in packet) {
        var item, opts = document.createElement("div");
        packet['options'].forEach(function (t) {
            item = document.createElement("div");
            item.innerHTML = t;
            opts.appendChild(item);
        });
        wrapper.appendChild(opts);
    }

    var seen_button = document.createElement("button");
    seen_button.innerHTML = "Ok";
    seen_button.addEventListener("click", on_click);
    wrapper.appendChild(seen_button);
}

function layout_manager_find_element(id, root) {
    if (root === undefined) {
        root = wrapper;
    }

    var counter = -1;
    while (root.children[++counter]) {
        if (root.children[counter].id === id) {
            console.log(root.children[counter].id);
            return root.children[counter];

        } else {
            var nested = layout_manager_find_element(id, root.children[counter]);
            if (nested !== undefined) return nested;
        }
    }

    return undefined;
}

function layout_manager_test() {
    layout_manager_init();

    layout_manager_show_overlay();

    layout_manager_update_overlay("Henk Superfrog");

    var packet = {
        "phrase": "Who would you like to eliminate?",
        "options": ["me", "you", "that guy", "henk"]
    };
    layout_manager_show_options(packet, function (clicked) {
        console.log(clicked);
    });

    packet = {
        "role": "wolf"
    };
    layout_manager_show_card(packet, function (e) {
        console.log(e);
    });
    console.log("Done");
}

layout_manager_test();
