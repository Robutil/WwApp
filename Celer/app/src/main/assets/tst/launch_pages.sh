#!/usr/bin/env bash

IP="192.168.178.20"
ADDRESS="$IP:58080/debug.html"
COMBINE = ""
for i in 0 1 2 3 4
do
    COMBINE="$COMBINE $ADDRESS"
done

chromium-browser --auto-open-devtools-for-tabs ${COMBINE}