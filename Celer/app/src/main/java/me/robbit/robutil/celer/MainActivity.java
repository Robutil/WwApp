package me.robbit.robutil.celer;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.net.UnknownHostException;

import me.robbit.robutil.celer.ww.core.GameModeFactory;
import me.robbit.robutil.celer.ww.core.ws.MyWebSockClient;
import me.robbit.robutil.celer.ww.core.Controller;
import me.robbit.robutil.celer.ww.activities.client.WwConfigClientActivity;
import me.robbit.robutil.celer.ww.activities.server.WwConfigServerActivity;
import robbit.net.webserver.LocalWebServer;
import robbit.net.websockets.LocalWebSockServer;

public class MainActivity extends AppCompatActivity {

    //Debug
    public static final String TAG = "ROB";

    //Offline mode
    private LocalWebServer localWebServer;
    private LocalWebSockServer localWebSockServer;

    //UI stuff
    private TextView textView;
    private Button buttonOne;
    private Button buttonTwo;

    //Logic
    private Controller controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Init UI elements
        textView = (TextView) findViewById(R.id.MainActivityTextView);
        buttonOne = (Button) findViewById(R.id.MainActivityButton1);
        buttonTwo = (Button) findViewById(R.id.MainActivityButton2);

        //This will change in the future
        buttonOne.setText("SERVER");
        buttonTwo.setText("CLIENT");

        buttonOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), WwConfigServerActivity.class);
                startActivity(intent);
            }
        });

        buttonTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), WwConfigClientActivity.class);
                startActivity(intent);
            }
        });

        //activateOfflineMode();
    }

    void activateOfflineMode() {
        //Create logic responsible for managing clients
        controller = new Controller(GameModeFactory.GAME_MODE_VANILLA, 2, null, 10); //Broken

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int webServerPort = 58080;
                    final int webSocketPort = 58081;

                    //Start Web server
                    localWebServer = new LocalWebServer(webServerPort, getAssets());
                    localWebServer.start();

                    //Wait till server has started
                    while (!localWebServer.isRunning()) try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    String msg = "WebServer available on: " + localWebServer.getAddress();
                    Message message = new Message();
                    message.obj = msg;
                    handler.sendMessage(message);
                    Log.d(TAG, msg);

                    //Start WebSocket server
                    localWebSockServer = new LocalWebSockServer(localWebServer.getRawAddress(), webSocketPort, MyWebSockClient.class);
                    localWebSockServer.registerWebSocketClientCallback(controller);
                    localWebSockServer.start();

                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    //Handler updates UI thread
    final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            textView.setText(textView.getText().toString() + "\n" + msg.obj);
        }
    };

    public void test(String message) {
        Message helper = handler.obtainMessage();
        helper.obj = message;
        handler.sendMessage(helper);
    }

    @Override
    protected void onDestroy() {
        if (localWebServer != null) localWebServer.stop();
        if (localWebSockServer != null) localWebSockServer.stop();

        super.onDestroy();
    }
}
