package me.robbit.robutil.celer.ww.core;

public interface GenericController {

    void onOpen(Client client);

    void onClose(Client client);

    void onError(Client client, Exception ex);

    void onMessage(Client client, String message);
}
