package me.robbit.robutil.celer.ww.activities.server.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import me.robbit.robutil.celer.R;

public class ConfigRoleAdapter extends ArrayAdapter<ConfigRoleModel> {

    private LayoutInflater inflater;

    public ConfigRoleAdapter(Activity activity, int resource, ArrayList<ConfigRoleModel> items) {
        super(activity, resource, items);
        inflater = activity.getLayoutInflater();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            row = inflater.inflate(R.layout.ww_config_roles_item, null);
            holder = new ViewHolder(row);

            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        ConfigRoleModel itemModel = getItem(position);
        holder.populateRow(itemModel);

        return row;
    }

    private class ViewHolder {
        private TextView roleCounter;
        private TextView name;

        ViewHolder(View view) {
            roleCounter = (TextView) view.findViewById(R.id.WwConfigRolesItemCounter);
            name = (TextView) view.findViewById(R.id.WwConfigRolesItemName);
        }

        void populateRow(ConfigRoleModel item) {

            if (item.getIndex() == -1) {
                roleCounter.setText("");
            } else {
                String index = Integer.toString(item.getIndex());
                roleCounter.setText(index);
            }

            name.setText(item.getRoleName());
        }
    }
}
