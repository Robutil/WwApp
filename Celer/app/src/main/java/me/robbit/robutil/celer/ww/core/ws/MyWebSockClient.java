package me.robbit.robutil.celer.ww.core.ws;

import android.util.Log;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;

import java.net.InetSocketAddress;

import me.robbit.robutil.celer.MainActivity;
import me.robbit.robutil.celer.ww.core.Client;
import me.robbit.robutil.celer.ww.core.Controller;
import robbit.net.websockets.WebSocketClient;

public class MyWebSockClient extends WebSocketClient implements Client {

    private Controller callback = null;
    private WebSocket socket = null;
    private String name;
    private volatile boolean mutex = false;

    public MyWebSockClient(WebSocket socket) {
        super(socket);
    }

    @Override
    public void onOpen(ClientHandshake handshake) {
        socket = super.getSocket();

        InetSocketAddress remote = socket.getRemoteSocketAddress();
        name = remote.getHostName() + ":" + remote.getPort();
        callback = (Controller) super.getCallback();
        callback.onOpen(this);
    }

    @Override
    public void onMessage(String message) {
        Log.d(MainActivity.TAG, "Recv (" + name + ") : " + message);
        callback.onMessage(this, message);
    }

    @Override
    public void onError(Exception exception) {
        Log.d("TAG", exception.toString());
        callback.onError(this, exception);
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        callback.onClose(this);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void sendMessage(String message) {
        while (mutex) ;

        mutex = true;
        socket.send(message);
        mutex = false;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MyWebSockClient)
            return ((Client) obj).getName().equals(getName());
        else return super.equals(obj);
    }
}
