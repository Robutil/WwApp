package me.robbit.robutil.celer.ww.core.models.stages;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import me.robbit.robutil.celer.ww.core.Controller;
import me.robbit.robutil.celer.ww.core.models.Player;
import me.robbit.robutil.celer.ww.core.models.roles.GenericRole;
import me.robbit.robutil.celer.ww.core.models.roles.WerewolfRole;

public class NightOneStage extends GenericStage {

    public NightOneStage(Controller controller) {
        super(controller);
    }

    private int playersReadyForNextStage = 0;

    @Override
    public void onMessage(Player player, String message) {
        try {
            player.getRole().handleMessage(getStageId(), new JSONObject(message), player);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        int temp = controller.getCountPlayersReady();
        if (temp != playersReadyForNextStage) {

            playersReadyForNextStage = temp;

            JSONObject response = new JSONObject();
            try {
                response.put("id", controller.PACKET_WAIT);
                response.put("msg", player.getUsername());
                response.put("current", playersReadyForNextStage);
                response.put("max", controller.alivePlayerCount);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            controller.announceIfReady(response.toString());

            if (playersReadyForNextStage == controller.alivePlayerCount) {
                markPlayerThatWerewolvesKilled();

                controller.activateNextStage();
            }
        }
    }

    private void markPlayerThatWerewolvesKilled() {
//        //Get all votes
//        HashMap<String, Integer> voteCounter = new HashMap<>();
//        for (Player player : controller.players) {
//            if (player.getRole().getName().equals(GenericRole.roleNameWerewolf) && player.isAlive()) {
//                WerewolfRole helper = (WerewolfRole) player.getRole();
//                try {
//                    int currentVotes = voteCounter.get(helper.target);
//                    voteCounter.put(helper.target, currentVotes + 1);
//                } catch (NullPointerException ex) {
//                    voteCounter.put(helper.target, 1);
//                }
//            }
//        }
//
//        //Mark player with the most votes
//        Iterator it = voteCounter.entrySet().iterator();
//        String playerNameWithMostVotes = "";
//        int votes = 0;
//        while (it.hasNext()) {
//            Map.Entry item = (Map.Entry) it.next();
//            if ((int) item.getValue() > votes) {
//                playerNameWithMostVotes = (String) item.getKey();
//                votes = (int) item.getValue();
//            }
//        }

        ArrayList<Player> werewolves = controller.findPlayersByRole(GenericRole.roleNameWerewolf);
        for (Player player : werewolves) {
            if (player.isAlive()) {
                Player target = controller.findPlayer(player.getRole().target);
                target.setMarkedForDeath(true, GenericRole.roleNameWerewolf);
            }
        }

//        Player playerToMark = controller.findPlayer(playerNameWithMostVotes);
//        playerToMark.setMarkedForDeath(true, GenericRole.roleNameWerewolf);
    }

    @Override
    public GenericStage getNextStage() {
        return new NightTwoStage(controller);
    }

    @Override
    public int getStageId() {
        return STAGE_NIGHT_ONE;
    }

    @Override
    public void run() {
        invokeRoleStageStart(getStageId());
    }
}
