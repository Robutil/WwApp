package me.robbit.robutil.celer.ww.core.models.roles;


import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.robbit.robutil.celer.MainActivity;
import me.robbit.robutil.celer.ww.core.models.Player;
import me.robbit.robutil.celer.ww.core.models.stages.GenericStage;

public class WitchRole extends GenericRole {
    private boolean saveUsed = false;
    private boolean killUsed = false;
    private boolean priest = false;
    private boolean apprentice = false;

    public String getName() {
        return GenericRole.roleNameWitch;
    }

    public void enableApprentice() {
        apprentice = true;
    }

    public void enablePriest() {
        priest = true;
    }

    @Override
    public void handelStageStart(int stageId) {
        if (stageId == GenericStage.STAGE_NIGHT_TWO) {
            handleStageStartNightTwo();

        } else super.handelStageStart(stageId);
    }

    private void handleStageStartNightTwo() {
        if (!saveUsed) {
            sendSavePacket();

        } else {
            sendInfoPacket();
        }
    }

    private void sendInfoPacket() {
        ArrayList<String> options = new ArrayList<>();
        String info = "";
        for (Player player : controller.players) {
            if (player.isMarkedForDeath() && player.isAlive() && player.getRoleWhichMarked().equals(GenericRole.roleNameWerewolf)) {
                options.add(player.getUsername());
                info += player.getUsername() + " died.<br>";
            }
        }

        JSONObject msg = new JSONObject();
        try {
            msg.put("id", controller.PACKET_INFO);
            msg.put("phrase", info);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        playerCallback.getClient().sendMessage(msg.toString());
    }

    private void sendSavePacket() {
        ArrayList<String> options = new ArrayList<>();
        for (Player player : controller.players) {
            if (player.isMarkedForDeath() && player.isAlive() && player.getRoleWhichMarked().equals(GenericRole.roleNameWerewolf))
                options.add(player.getUsername());
        }
        options.add("Save nobody");

        JSONObject msg = new JSONObject();
        try {
            msg.put("id", controller.PACKET_OPTS);
            msg.put("phrase", "Do you want to save this player?");
            msg.put("options", new JSONArray(options));
            msg.put("type", "save"); //Client should reply with type
        } catch (JSONException e) {
            e.printStackTrace();
        }

        playerCallback.getClient().sendMessage(msg.toString());
    }

    private void sendKillPacket() {
        ArrayList<String> options = new ArrayList<>();
        for (Player player : controller.players) {
            if (player.isAlive() && !player.getUsername().equals(playerCallback.getUsername())) {
                options.add(player.getUsername());
            }
        }
        options.add("Kill nobody");

        JSONObject msg = new JSONObject();
        try {
            msg.put("id", controller.PACKET_OPTS);
            msg.put("phrase", "Do you want to kill any player?");
            msg.put("options", new JSONArray(options));
            msg.put("type", "kill"); //Client should reply with type
        } catch (JSONException e) {
            e.printStackTrace();
        }

        playerCallback.getClient().sendMessage(msg.toString());
    }

    @Override
    public void handleMessage(int stageId, JSONObject msg, Player origin) throws JSONException {
        if (stageId == GenericStage.STAGE_NIGHT_TWO) {
            handleMessageNightTwo(msg, origin);

        } else super.handleMessage(stageId, msg, origin);
    }

    public void handleMessageNightTwo(JSONObject msg, Player origin) throws JSONException {
        if (msg.getInt("id") == controller.PACKET_GENERIC) {

            String type = "save";
            if (msg.has("type")) type = msg.getString("type");

            Player pl = null;
            if (msg.has("answer")) pl = controller.findPlayer(msg.getString("answer"));

            if (type.equals("save")) {
                if (pl != null) {
                    pl.setMarkedForDeath(false, GenericRole.roleNameWitch);
                    saveUsed = true;
                    if (apprentice) killUsed = true;
                }

                if (!killUsed && !priest) sendKillPacket();
                else playerCallback.ready = true;

            } else if (type.equals("kill")) {
                playerCallback.ready = true;

                if (pl != null) {
                    pl.setMarkedForDeath(true, GenericRole.roleNameWitch);
                    killUsed = true;

                    if (apprentice) saveUsed = true;
                }
            }

        }
    }
}
