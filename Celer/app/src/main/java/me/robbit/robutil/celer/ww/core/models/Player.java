package me.robbit.robutil.celer.ww.core.models;

import me.robbit.robutil.celer.ww.core.Client;
import me.robbit.robutil.celer.ww.core.Controller;
import me.robbit.robutil.celer.ww.core.models.roles.GenericRole;

public class Player {

    private final Client client;
    private final Controller controller;

    private String username = null;
    private GenericRole role = null;

    public boolean ready = false;
    private boolean alive = true;
    private boolean markedForDeath = false;
    private String roleWhichMarked = null;

    public Player(Client client, Controller controller) {
        this.client = client;
        this.controller = controller;
    }

    public Client getClient() {
        return client;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public GenericRole getRole() {
        return role;
    }

    public void setRole(GenericRole role) {
        this.role = role;
        role.registerCallback(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Player) {
            return ((Player) obj).getUsername() != null && ((Player) obj).getUsername().equals(username);
        } else if (obj instanceof Client) {
            return obj.equals(this.client);
        }
        return super.equals(obj);
    }

    public Controller getController() {
        return controller;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public boolean isMarkedForDeath() {
        return markedForDeath;
    }

    public String getRoleWhichMarked() {
        return roleWhichMarked;
    }

    public void setMarkedForDeath(boolean markedForDeath, String roleNameSetter) {
        this.markedForDeath = markedForDeath;
        roleWhichMarked = roleNameSetter;
    }
}
