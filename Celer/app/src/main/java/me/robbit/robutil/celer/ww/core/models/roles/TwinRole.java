package me.robbit.robutil.celer.ww.core.models.roles;

public class TwinRole extends GenericRole {
    @Override
    public String getName() {
        return GenericRole.roleNameTwin;
    }
}
