package me.robbit.robutil.celer.ww.activities.server.utils;

public class ConfigRoleModel {

    private int index;
    private String roleName;
    private String roleDescription;

    public ConfigRoleModel(String roleName) {
        this.roleName = roleName;
    }

    public ConfigRoleModel(int index, String roleName) {
        this.index = index;
        this.roleName = roleName;
    }

    public ConfigRoleModel(int index, String roleName, String roleDescription) {
        this.index = index;
        this.roleName = roleName;
        this.roleDescription = roleDescription;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleDescription() {
        return roleDescription;
    }

    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }
}
