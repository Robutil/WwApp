package me.robbit.robutil.celer.ww.core.models.stages;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import me.robbit.robutil.celer.ww.core.Controller;
import me.robbit.robutil.celer.ww.core.models.Player;

public class VoteStage extends GenericStage {

    private HashMap<String, Integer> allVotes = new HashMap<>();
    private boolean voting = true;
    private int playersThatInteracted = 0;

    private String log = "";

    public VoteStage(Controller controller) {
        super(controller);
    }

    @Override
    public void onMessage(Player player, String message) {
        try {
            JSONObject msg = new JSONObject(message);
            int packetId = msg.getInt("id");

            if (packetId == controller.PACKET_GENERIC && player.isAlive()) {
                if (voting) handleMessageVote(player, msg);
                else handleMessageInfoSeen(player, msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleMessageVote(Player player, JSONObject msg) throws JSONException {
        playersThatInteracted++;
        player.ready = true;

        //Find out target, and keep log to inform players later
        String target = msg.getString("answer");
        log += player.getUsername() + " voted for " + target + "<br>";

        //Keep track of votes
        int voteCounter = 1;
        if (allVotes.containsKey(target)) voteCounter = allVotes.get(target) + 1;
        allVotes.put(target, voteCounter);

        //Inform waiting players
        JSONObject waitMsg = generateWaitPacket("Waiting for players", playersThatInteracted, controller.alivePlayerCount);
        controller.announceIfReady(waitMsg.toString());

        if (playersThatInteracted == controller.alivePlayerCount) {
            voting = false;
            resetPlayersReady();
            playersThatInteracted = 0;

            killMostVotedPlayer();
            sendLogMessage();
        }
    }

    private void killMostVotedPlayer() {
        //Get most voted player
        String currentTarget = "";
        int currentVotes = 0;
        boolean duplicateVote = false;
        Iterator it = allVotes.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            if ((int) pair.getValue() > currentVotes) {
                currentTarget = (String) pair.getKey();
                currentVotes = (int) pair.getValue();
                duplicateVote = false;
            } else if ((int) pair.getValue() == currentVotes) {
                duplicateVote = true;
            }
            it.remove();
        }

        if (!duplicateVote) {
            //Kill most voted player
            Player mostVotedPlayer = controller.findPlayer(currentTarget);
            controller.killPlayer(mostVotedPlayer);

            //Update log
            log += currentTarget + " died, his role was: " + mostVotedPlayer.getRole().getName() + "<br>";

        } else {
            log += "Nobody died, there was an equal vote";
        }
    }

    private void sendLogMessage() throws JSONException {
        JSONObject logMessage = new JSONObject();
        logMessage.put("id", controller.PACKET_INFO);
        logMessage.put("phrase", log);
        controller.announce(logMessage.toString());
    }

    private void handleMessageInfoSeen(Player player, JSONObject msg) {
        playersThatInteracted++;
        player.ready = true;

        JSONObject waitMsg = generateWaitPacket("Waiting for players", playersThatInteracted, controller.alivePlayerCount);
        controller.announceIfReady(waitMsg.toString());

        if (playersThatInteracted == controller.alivePlayerCount) {
            controller.activateNextStage();
        }
    }

    @Override
    public GenericStage getNextStage() {
        return new NightOneStage(controller);
    }

    @Override
    public int getStageId() {
        return GenericStage.STAGE_VOTE;
    }

    @Override
    public void run() {
        //Get list of alive players
        ArrayList<Player> alivePlayers = new ArrayList<>();
        for (Player player : controller.players) {
            if (player.isAlive()) alivePlayers.add(player);
        }

        //Broadcast vote options
        for (Player player : alivePlayers) {

            //Remove own name from vote options
            ArrayList<Player> voteOptions = (ArrayList<Player>) alivePlayers.clone();
            voteOptions.remove(player);

            //Transform player objects to array containing user names
            ArrayList<String> voteOptionsByName = new ArrayList<>();
            for (Player opt : voteOptions) {
                voteOptionsByName.add(opt.getUsername());
            }

            JSONObject votePacket = new JSONObject();
            try {
                votePacket.put("id", controller.PACKET_OPTS);
                votePacket.put("phrase", "Vote to kill");
                votePacket.put("options", new JSONArray(voteOptionsByName));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            player.getClient().sendMessage(votePacket.toString());
        }

    }
}
