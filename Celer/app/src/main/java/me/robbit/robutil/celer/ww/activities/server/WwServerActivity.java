package me.robbit.robutil.celer.ww.activities.server;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.net.UnknownHostException;

import me.robbit.robutil.celer.MainActivity;
import me.robbit.robutil.celer.R;
import me.robbit.robutil.celer.ww.core.GameModeFactory;
import me.robbit.robutil.celer.ww.core.ws.MyWebSockClient;
import me.robbit.robutil.celer.ww.bridge.BridgeServer;
import me.robbit.robutil.celer.ww.core.Controller;
import robbit.net.webserver.LocalWebServer;
import robbit.net.websockets.LocalWebSockServer;

public class WwServerActivity extends AppCompatActivity {

    private BridgeServer bridge = null;
    private Controller controller;
    private LocalWebServer localWebServer;
    private LocalWebSockServer localWebSockServer;

    private TextView textView;
    private Button button;
    private ImageView imageView;

    private String address = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ww_server);

        Intent intent = getIntent();

        boolean activateBluetoothBridge = intent.getBooleanExtra(WwConfigServerActivity.INTENT_BRIDGE, false);
        int playerCount = intent.getIntExtra(WwConfigServerActivity.INTENT_PLAYERS, -1);
        String[] roles = intent.getStringArrayExtra(WwConfigServerActivity.INTENT_ROLES);
        int dayDurationInSeconds = intent.getIntExtra(WwConfigServerActivity.INTENT_DAY_DURATION, 10);
        String gameMode = intent.getStringExtra(WwConfigServerActivity.INTENT_GAME_MODE);

        Log.d(MainActivity.TAG, "Found player count: " + Integer.toString(playerCount));
        Log.d(MainActivity.TAG, "Found day duration: " + Integer.toString(dayDurationInSeconds));

        if (playerCount == -1) throw new RuntimeException("Illegal player count");

        GameModeFactory gameModeFactory = new GameModeFactory();
        int gameModeId = gameModeFactory.getGameModeIndex(gameMode);
        controller = new Controller(gameModeId, playerCount, roles, dayDurationInSeconds);

        textView = (TextView) findViewById(R.id.WwServerActivityText);
        button = (Button) findViewById(R.id.WwServerActivityButton);
        imageView = (ImageView) findViewById(R.id.WwServerActivityImage);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (address != null) {
                    Intent browser = new Intent(Intent.ACTION_VIEW);
                    browser.setData(Uri.parse(address));
                    startActivityForResult(browser, 0);
                    button.setVisibility(View.INVISIBLE);
                }
            }
        });

        start();
        if (activateBluetoothBridge) startBluetoothServerBridge();
    }

    private void startBluetoothServerBridge() {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }

        bridge = new BridgeServer();
        bridge.start(controller);
    }

    private void start() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    int webServerPort = 58080;
                    final int webSocketPort = 58081;

                    //Start Web server
                    localWebServer = new LocalWebServer(webServerPort, getAssets());
                    localWebServer.start();

                    //Wait till server has started
                    while (!localWebServer.isRunning()) try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    //Publish URL
                    postToUiThread(localWebServer.getAddress());

                    //Start WebSocket server
                    localWebSockServer = new LocalWebSockServer(localWebServer.getRawAddress(), webSocketPort, MyWebSockClient.class);
                    localWebSockServer.registerWebSocketClientCallback(controller);
                    localWebSockServer.start();

                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    //Handler updates UI thread
    @SuppressLint("HandlerLeak")
    final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            address = "http://" + msg.obj;
            textView.setText(address);

            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
            try {
                BitMatrix bitMatrix = multiFormatWriter.encode(address, BarcodeFormat.QR_CODE, 200, 200);
                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);

                imageView.setImageBitmap(bitmap);
                imageView.setAdjustViewBounds(true);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            } catch (WriterException e) {
                e.printStackTrace();
            }
        }
    };

    public void postToUiThread(String message) {
        Message helper = handler.obtainMessage();
        helper.obj = message;
        handler.sendMessage(helper);
    }

    @Override
    protected void onDestroy() {
        Log.d(MainActivity.TAG, "[WwServerActivity](onDestroy): On destroy called!");

        if (bridge != null) bridge.stop();
        if (localWebServer != null) localWebServer.stop();
        if (localWebSockServer != null) localWebSockServer.stop();

        super.onDestroy();

    }
}
