package me.robbit.robutil.celer.ww.activities.client;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.net.UnknownHostException;

import me.robbit.robutil.celer.R;
import me.robbit.robutil.celer.ww.core.ws.MyWebSockClient;
import me.robbit.robutil.celer.ww.bridge.BridgeClient;
import me.robbit.robutil.celer.ww.bridge.FakeController;
import robbit.net.webserver.LocalWebServer;
import robbit.net.websockets.LocalWebSockServer;

public class WwClientActivity extends AppCompatActivity {

    private BridgeClient peer;
    private FakeController controller;

    private LocalWebServer localWebServer;
    private LocalWebSockServer localWebSockServer;

    private TextView textView;
    private Button button;
    private String address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ww_client);

        textView = (TextView) findViewById(R.id.WwClientActivityText);
        button = (Button) findViewById(R.id.WwClientActivityButton);

        peer = new BridgeClient();
        controller = new FakeController(peer);

        BluetoothDevice device = getIntent().getParcelableExtra("device");
        peer.start(device, controller);
        BluetoothAdapter.getDefaultAdapter().cancelDiscovery();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (address != null) {
                    Intent browser = new Intent(Intent.ACTION_VIEW);
                    browser.setData(Uri.parse(address));
                    startActivity(browser);
                    button.setVisibility(View.GONE);
                }
            }
        });

        start();
    }

    private void start() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    int webServerPort = 58080;
                    final int webSocketPort = 58081;

                    //Start Web server
                    localWebServer = new LocalWebServer(webServerPort, getAssets());
                    localWebServer.start();

                    //Wait till server has started
                    while (!localWebServer.isRunning()) try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    //Publish URL
                    postToUiThread(localWebServer.getAddress());

                    //Start WebSocket server
                    localWebSockServer = new LocalWebSockServer(localWebServer.getRawAddress(), webSocketPort, MyWebSockClient.class);
                    localWebSockServer.registerWebSocketClientCallback(controller);
                    localWebSockServer.start();

                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    //Handler updates UI thread
    @SuppressLint("HandlerLeak")
    final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            address = "http://" + msg.obj + "index.html";
            textView.setText(address);
        }
    };

    public void postToUiThread(String message) {
        Message helper = handler.obtainMessage();
        helper.obj = message;
        handler.sendMessage(helper);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (peer != null) peer.stop();
        if (localWebServer != null) localWebServer.stop();
        if (localWebSockServer != null) localWebSockServer.stop();
    }
}
