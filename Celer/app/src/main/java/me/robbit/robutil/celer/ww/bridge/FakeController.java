package me.robbit.robutil.celer.ww.bridge;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.robbit.robutil.celer.MainActivity;
import me.robbit.robutil.celer.ww.core.Client;
import me.robbit.robutil.celer.ww.core.GenericController;

import static me.robbit.robutil.celer.ww.bridge.BridgeServer.CLIENT_CLOSED;
import static me.robbit.robutil.celer.ww.bridge.BridgeServer.CLIENT_CONNECT;
import static me.robbit.robutil.celer.ww.bridge.BridgeServer.CLIENT_MESSAGE;

public class FakeController implements GenericController {

    private ArrayList<Client> clients;
    private BridgeClient bridge;

    public FakeController(BridgeClient bridge) {
        this.bridge = bridge;
        clients = new ArrayList<>();
    }

    @Override
    public void onOpen(Client client) {
        clients.add(client);
        if (!client.getName().equals("peer")) notifyClientOpen(client);
        else Log.d(MainActivity.TAG, "FakeController: Succesful Bluetooth connection!");
    }

    @Override
    public void onClose(Client client) {
        clients.remove(client);
        if (!client.getName().equals("peer")) notifyClientClose(client);
    }

    @Override
    public void onError(Client client, Exception ex) {
        clients.remove(client);
        if (!client.getName().equals("peer")) notifyClientClose(client);
    }

    @Override
    public void onMessage(Client client, String message) {
        if (client.getName().equals("peer")) handleMessageFromPeer(client, message);
        else forwardMessageToPeer(client, message);
    }

    private void notifyClientOpen(Client client) {
        try {
            JSONObject wrapper = new JSONObject();
            wrapper.put("id", CLIENT_CONNECT);
            wrapper.put("origin", client.getName());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void notifyClientClose(Client client) {
        try {
            JSONObject wrapper = new JSONObject();
            wrapper.put("id", CLIENT_CLOSED);
            wrapper.put("origin", client.getName());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleMessageFromPeer(Client client, String message) {
        try {
            JSONObject wrapper = new JSONObject(message);

            int msgType = wrapper.getInt("id");

            if (msgType == CLIENT_MESSAGE) {
                String target = wrapper.getString("target");
                String msg = wrapper.getString("message");

                for (Client cl : clients) {
                    if (cl.getName().equals(target)) {
                        cl.sendMessage(msg);
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void forwardMessageToPeer(Client client, String message) {
        try {
            JSONObject wrapper = new JSONObject();
            wrapper.put("id", CLIENT_MESSAGE);
            wrapper.put("origin", client.getName());
            wrapper.put("msg", message);

            bridge.sendMessage(wrapper.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
