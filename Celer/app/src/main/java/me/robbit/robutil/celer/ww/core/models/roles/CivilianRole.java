package me.robbit.robutil.celer.ww.core.models.roles;

import java.util.ArrayList;

import me.robbit.robutil.celer.ww.core.Controller;
import me.robbit.robutil.celer.ww.core.models.Player;
import me.robbit.robutil.celer.ww.core.models.stages.GenericStage;

public class CivilianRole extends GenericRole {

    @Override
    public String getName() {
        return GenericRole.roleNameCivilian;
    }
}
