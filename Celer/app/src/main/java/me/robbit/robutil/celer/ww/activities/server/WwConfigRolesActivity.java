package me.robbit.robutil.celer.ww.activities.server;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import me.robbit.robutil.celer.MainActivity;
import me.robbit.robutil.celer.R;
import me.robbit.robutil.celer.ww.activities.server.utils.ConfigRoleAdapter;
import me.robbit.robutil.celer.ww.activities.server.utils.ConfigRoleModel;
import me.robbit.robutil.celer.ww.core.models.roles.GenericRole;
import me.robbit.robutil.celer.ww.core.models.roles.RoleFactory;

import static me.robbit.robutil.celer.ww.activities.server.WwConfigServerActivity.INTENT_GAME_MODE;

public class WwConfigRolesActivity extends AppCompatActivity {

    private ArrayList<ConfigRoleModel> roleModels;
    private ConfigRoleAdapter adapter;
    private RoleFactory roleFactory = new RoleFactory();

    private String selectedGameMode;
    private int INTENT_ADD_ROLE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ww_config_roles);
        Intent intent = getIntent();

        selectedGameMode = intent.getStringExtra(WwConfigServerActivity.INTENT_GAME_MODE);

        roleModels = new ArrayList<>();
        String[] currentRoles = intent.getStringArrayExtra(WwConfigServerActivity.INTENT_ROLES);
        for (String role : currentRoles) {
            addRole(role, false, true);
        }

        final ListView listView = (ListView) findViewById(R.id.WwConfigRolesActivityListView);
        final Button addButton = (Button) findViewById(R.id.WwConfigRolesActivityAddRole);
        Button doneButton = (Button) findViewById(R.id.WwConfigRolesActivityButton);

        adapter = new ConfigRoleAdapter(this, R.layout.ww_config_roles_item, roleModels);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final ConfigRoleModel item = (ConfigRoleModel) listView.getItemAtPosition(position);
                Log.d(MainActivity.TAG, "Item clicked: " + item.getRoleName());

                AlertDialog.Builder builder = new AlertDialog.Builder(WwConfigRolesActivity.this);
                builder.setTitle("Role information");
                builder.setMessage(item.getRoleDescription());
                builder.setPositiveButton("Ok", null);

                builder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        roleModels.remove(item);

                        if (item.getRoleName().equals(GenericRole.roleNameTwin)) {
                            for (ConfigRoleModel m : roleModels) {
                                if (m.getRoleName().equals(GenericRole.roleNameTwin)) {
                                    roleModels.remove(m);
                                }
                            }
                        }

                        updateCounters();
                        adapter.notifyDataSetChanged();
                    }
                });
                builder.show();
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent addRoleIntent = new Intent(WwConfigRolesActivity.this, WwConfigAddRoleActivity.class);
                addRoleIntent.putExtra(INTENT_GAME_MODE, selectedGameMode);
                startActivityForResult(addRoleIntent, INTENT_ADD_ROLE);
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                String[] roleNames = new String[roleModels.size()];
                for (int i = 0; i < roleModels.size(); i++) {
                    roleNames[i] = roleModels.get(i).getRoleName();
                }
                returnIntent.putExtra(WwConfigServerActivity.INTENT_ROLES, roleNames);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == INTENT_ADD_ROLE) {
                String role = data.getStringExtra(WwConfigAddRoleActivity.INTENT_CHOSEN_ROLE);
                addRole(role);
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void addRole(String name) {
        addRole(name, true);
    }

    private void addRole(String name, boolean update) {
        addRole(name, update, false);
    }

    private void addRole(String name, boolean update, boolean firstRun) {
        int numberOfTimesToAdd = 1;
        if (name.equals(GenericRole.roleNameTwin) && !firstRun) numberOfTimesToAdd = 2;

        for (int i = 0; i < numberOfTimesToAdd; i++) {
            ConfigRoleModel model = new ConfigRoleModel(name);
            model.setRoleDescription(roleFactory.getRoleDescription(name));
            roleModels.add(model);
        }

        updateCounters();
        if (update) adapter.notifyDataSetChanged();
    }

    private void updateCounters() {
        int counter = 1;
        for (ConfigRoleModel item : roleModels) {
            item.setIndex(counter++);
        }
    }
}
