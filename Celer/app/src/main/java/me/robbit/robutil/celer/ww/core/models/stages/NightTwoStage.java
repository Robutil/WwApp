package me.robbit.robutil.celer.ww.core.models.stages;

import org.json.JSONException;
import org.json.JSONObject;

import me.robbit.robutil.celer.ww.core.Controller;
import me.robbit.robutil.celer.ww.core.models.Player;

public class NightTwoStage extends GenericStage {

    public NightTwoStage(Controller controller) {
        super(controller);
    }

    private int playersReadyForNextStage = 0;

    @Override
    public void onMessage(Player player, String message) {
        try {
            player.getRole().handleMessage(getStageId(), new JSONObject(message), player);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (player.ready) {
            playersReadyForNextStage++;

            JSONObject response = new JSONObject();
            try {
                response.put("id", controller.PACKET_WAIT);
                response.put("msg", player.getUsername());
                response.put("current", playersReadyForNextStage);
                response.put("max", controller.alivePlayerCount);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            controller.announceIfReady(response.toString());

            if (playersReadyForNextStage == controller.alivePlayerCount) {
                controller.activateNextStage();
            }
        }
    }

    @Override
    public GenericStage getNextStage() {
        return new NightResultStage(controller);
    }

    @Override
    public int getStageId() {
        return STAGE_NIGHT_TWO;
    }

    @Override
    public void run() {
        invokeRoleStageStart(getStageId());
    }
}
