package me.robbit.robutil.celer.ww.core.models.roles;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.robbit.robutil.celer.MainActivity;
import me.robbit.robutil.celer.ww.core.models.Player;
import me.robbit.robutil.celer.ww.core.models.stages.GenericStage;

public class WerewolfRole extends GenericRole {

    @Override
    public String getName() {
        return GenericRole.roleNameWerewolf;
    }

    @Override
    public void handelStageStart(int stageId) {
        if (stageId == GenericStage.STAGE_NIGHT_ONE) {
            handleStageStartNightOne();

        } else super.handelStageStart(stageId);
    }

    private void handleStageStartNightOne() {
        this.target = "";

        ArrayList<String> options = new ArrayList<>();
        for (Player player : controller.players) {
            if (player.isAlive() && !player.getRole().getName().equals(GenericRole.roleNameWerewolf)) {
                options.add(player.getUsername());
            }
        }

        JSONObject msg = new JSONObject();
        try {
            msg.put("id", controller.PACKET_OPTS);
            msg.put("phrase", "Pick the target you want to eliminate");
            msg.put("options", new JSONArray(options));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        playerCallback.getClient().sendMessage(msg.toString());
    }

    @Override
    public void handleMessage(int stageId, JSONObject msg, Player origin) throws JSONException {
        if (stageId == GenericStage.STAGE_NIGHT_ONE) {
            handleMessageNightOne(msg, origin);

        } else super.handleMessage(stageId, msg, origin);
    }

    public void handleMessageNightOne(JSONObject msg, Player origin) throws JSONException {
        if (msg.getInt("id") == controller.PACKET_GENERIC && playerCallback.isAlive()) {
            String target = msg.getString("answer");

            JSONObject reply = new JSONObject();
            reply.put("id", controller.PACKET_GENERIC);
            reply.put("name", playerCallback.getUsername());
            reply.put("answer", target);

            this.target = target;

            ArrayList<Player> allWerewolves = controller.findPlayersByRole(GenericRole.roleNameWerewolf);
            int counter = 0;

            ArrayList<Player> allies = new ArrayList<>();
            //Do not mind dead players
            for (Player ally : allWerewolves) {
                if (ally.isAlive()) {
                    allies.add(ally);
                }
            }

            //Send message to player and every ally
            for (Player player : allies) {
                player.getClient().sendMessage(reply.toString());
                Log.d(MainActivity.TAG, "My target: " + this.target + " ally target: " + player.getRole().target);

                if (player.getRole().target.equals(this.target)) {
                    counter++;
                    Log.d(MainActivity.TAG, "Incremented counter: " + Integer.toString(counter));
                }
            }

            //If all werewolves vote the same target, all are ready
            if (counter == allies.size()) {
                Log.d(MainActivity.TAG, "Werewolves ready: " + Integer.toString(allies.size()));
                for (Player player : allies) {
                    player.ready = true;
                }
            }

        }
    }
}
