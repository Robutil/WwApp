package me.robbit.robutil.celer.ww.core;

public class GameModeFactory {

    private final int GAME_MODE_NOT_FOUND = -1;

    public static final int GAME_MODE_VANILLA = 0;
    public static final int GAME_MODE_ARYE = 1;
    public static final int GAME_MODE_WANK = 2;

    private static final String[][] gameModesAndDescriptions = {
            {
                    "Vanilla", "Murder"
            },
            {
                    "ARYE", "We're quite the experts when it comes to Weerwolven, let's add a" +
                    " difficulty factor. \n From all roles a selection is made at random, with " +
                    "the amount of werewolves fixed. This way no one really knows what roles are in " +
                    "play.\nSo when you have X players, there are X+3 roles in the pool FOR INSTANCE. "
            },
            {
                    "WANK", "Let's face it: do you ever really know someone? Whenever someone is " +
                    "killed (or only vote/werewolf murder?) their identity is not made known. When " +
                    "the last werewolf dies, obviously it is made known that the citizens have won." +
                    "\nNote: you will want the Gravedigger in this game mode."
            }
    };

    public static String getGameModeName(int gameMode) {
        return gameModesAndDescriptions[gameMode][0];
    }

    public String[] getGameModeNames() {
        String[] gameModeNames = new String[gameModesAndDescriptions.length];

        for (int i = 0; i < gameModesAndDescriptions.length; i++) {
            gameModeNames[i] = gameModesAndDescriptions[i][0];
        }
        return gameModeNames;
    }

    public String getGameModeDescription(String gameModeName) {
        int index = getGameModeIndex(gameModeName);
        if (index == GAME_MODE_NOT_FOUND) return null;

        else return gameModesAndDescriptions[index][1];

    }

    public int getGameModeIndex(String name) {
        for (int i = 0; i < gameModesAndDescriptions.length; i++) {
            if (gameModesAndDescriptions[i][0].equals(name)) return i;
        }
        return GAME_MODE_NOT_FOUND;
    }
}
