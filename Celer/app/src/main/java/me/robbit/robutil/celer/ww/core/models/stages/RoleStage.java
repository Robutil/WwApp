package me.robbit.robutil.celer.ww.core.models.stages;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.robbit.robutil.celer.MainActivity;
import me.robbit.robutil.celer.ww.core.Controller;
import me.robbit.robutil.celer.ww.core.models.Player;
import me.robbit.robutil.celer.ww.core.models.roles.GenericRole;
import me.robbit.robutil.celer.ww.core.models.roles.NoblemanRole;
import me.robbit.robutil.celer.ww.core.models.roles.RoleFactory;

public class RoleStage extends GenericStage {

    private int playersWhoCheckedRole = 0;

    public RoleStage(Controller controller) {
        super(controller);
    }

    @Override
    public void onMessage(Player player, String message) {
        try {
            JSONObject msg = new JSONObject(message);
            int packetId = msg.getInt("id");
            Log.d(MainActivity.TAG, "Got new message, id: " + packetId);

            if (packetId == controller.PACKET_GENERIC) handleRoleSeen(player, msg);

        } catch (JSONException e) {
            Log.d(MainActivity.TAG, "Controller: could not parse msg: " + e.toString());
        }
    }

    private void handleRoleSeen(Player player, JSONObject msg) {
        playersWhoCheckedRole++;
        player.ready = true;

        JSONObject response = new JSONObject();
        try {
            response.put("id", controller.PACKET_WAIT);
            response.put("msg", player.getUsername());
            response.put("current", playersWhoCheckedRole);
            response.put("max", controller.alivePlayerCount);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        player.getClient().sendMessage(response.toString());

        Log.d(MainActivity.TAG, response.toString());
        controller.announceIfReady(response.toString());

        if (playersWhoCheckedRole == controller.alivePlayerCount) {
            controller.activateNextStage();
        }
    }

    @Override
    public GenericStage getNextStage() {
        return new NightOneStage(controller);
    }

    @Override
    public int getStageId() {
        return STAGE_ROLE;
    }

    @Override
    public void run() {
        resetPlayersReady();
        RoleFactory roleFactory = new RoleFactory(controller.gameMode, controller.alivePlayerCount, controller.rolesByName);

        assignRoles(roleFactory);
        notifyRoles(roleFactory);
    }

    private void assignRoles(RoleFactory roleFactory) {
        for (Player player : controller.players) {
            player.setRole(roleFactory.getNextRandomRole());
        }
    }

    private void notifyRoles(RoleFactory roleFactory) {
        ArrayList<Player> nobles = controller.findPlayersByRole(GenericRole.roleNameNobleman);

        for (Player player : controller.players) {
            JSONObject msg = new JSONObject();
            try {
                String extraInfo = "";
                msg.put("id", controller.PACKET_CARD);
                msg.put("role", player.getRole().getName());
                msg.put("description", roleFactory.getRoleDescription(player.getRole().getName()));

                extraInfo += handlePossibleWerewolfAllies(player);
                extraInfo += handlePossibleTwin(player);
                extraInfo += handlePossibleServant(nobles, player);

                msg.put("info", extraInfo);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            player.getClient().sendMessage(msg.toString());
        }
    }

    private String handlePossibleWerewolfAllies(Player player) {
        if (!player.getRole().getName().equals(GenericRole.roleNameWerewolf)) return "";

        ArrayList<Player> wereWolfAllies = controller.findPlayersByRole(GenericRole.roleNameWerewolf);

        String info = "";
        for (Player ally : wereWolfAllies) {
            if (ally != player) info += ally.getUsername() + " is a werewolf.<br>";
        }
        return info;
    }

    private String handlePossibleTwin(Player player) {
        if (player.getRole().getName().equals(GenericRole.roleNameTwin)) {
            for (Player possibleTwin : controller.players) {
                if (possibleTwin.getRole().getName().equals(GenericRole.roleNameTwin) && possibleTwin != player) {
                    return "Your twin is: " + possibleTwin.getUsername() + ".<br>";
                }
            }
        }

        //Player is a twin, but there is no other twin in the game
        if (player.getRole().getName().equals(GenericRole.roleNameTwin)) {
            return "You twin does not exist.<br>";
        }
        return "";
    }

    private String handlePossibleServant(ArrayList<Player> nobles, Player player) {
        for (Player noble : nobles) {
            ArrayList<Player> servants = ((NoblemanRole) noble.getRole()).getServants();
            if (servants.contains(player)) {
                return noble.getUsername() + " is a nobleman.<br>";
            }
        }
        return "";
    }
}
