package me.robbit.robutil.celer.ww.core;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.robbit.robutil.celer.MainActivity;
import me.robbit.robutil.celer.ww.core.models.roles.GenericRole;
import me.robbit.robutil.celer.ww.core.models.stages.GenericStage;
import me.robbit.robutil.celer.ww.core.models.stages.PlayerJoinStage;
import me.robbit.robutil.celer.ww.core.models.Player;

public class Controller implements GenericController {

    public int gameMode;
    public int alivePlayerCount;
    public volatile int currentNumberOfPlayers = 0;
    public boolean gameActive = true;

    public final int PACKET_SYS_PING = 1;
    public final int PACKET_SYS_SOUND_REQUEST = 2;
    public final int PACKET_SYS_PLAYER_STATUS = 3;

    public final int PACKET_GENERIC = 1000;
    public final int PACKET_USERNAME = 1001;
    public final int PACKET_WAIT = 1003;
    public final int PACKET_CARD = 1005;
    public final int PACKET_OPTS = 1007;
    public final int PACKET_INFO = 1009;
    public final int PACKET_AUDIO = 1011;

    public final int PACKET_STAGE_ANNOUNCE = 1004;

    public ArrayList<Player> players;
    public final String[] rolesByName;
    public final int dayDurationInSeconds;

    private GenericStage activeStage;
    private boolean rolesDistributed;

    public Controller(int gameMode, int alivePlayerCount, String[] rolesByName, int dayDurationInSeconds) {
        this.gameMode = gameMode;
        this.alivePlayerCount = alivePlayerCount;
        players = new ArrayList<>();
        this.rolesByName = rolesByName;
        this.dayDurationInSeconds = dayDurationInSeconds;

        activeStage = new PlayerJoinStage(this);
    }

    @Override
    public void onOpen(Client client) {
        Log.d(MainActivity.TAG, "Controller: New client: " + client.getName());
        players.add(new Player(client, this));
        //sendPlayerStatusUpdate();
    }

    @Override
    public void onClose(Client client) {
        Log.d(MainActivity.TAG, "Controller: Close client: " + client.getName());
        players.remove(findPlayer(client));
    }

    @Override
    public void onError(Client client, Exception ex) {
        ex.printStackTrace();
        Log.d(MainActivity.TAG, "Controller: Client [" + client.getName() + "] closed connection: " + ex.toString());
        players.remove(findPlayer(client));
    }

    @Override
    public void onMessage(Client client, String message) {
        try {
            JSONObject sysMessage = new JSONObject(message);
            int id = sysMessage.getInt("id");

            if (id == PACKET_SYS_PING) {
                //Reply with original message
                client.sendMessage(message);
                Log.d(MainActivity.TAG, "[Controller](onMessage): Heartbeat: " + client.getName());
                return;
            } else if (id == PACKET_SYS_SOUND_REQUEST) {
                String audioFileName = sysMessage.getString("phrase");
                announceAudioMessagePrivate(findPlayer(client), audioFileName);
                return;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        Player player = findPlayer(client);
        if (player == null) throw new RuntimeException("Unknown connection");

        Log.d(MainActivity.TAG, "Got: " + message + ", from: " + client.getName());

        Log.d(MainActivity.TAG, "ActiveStage: " + activeStage.getStageId());
        if (gameActive) activeStage.onMessage(player, message);
    }

    public void activateNextStage() {
        Log.d(MainActivity.TAG, "[Controller](activateNextStage): Activating next stage");
        if (activeStage.getStageId() == GenericStage.STAGE_ROLE) {
            rolesDistributed = true;
        }

        debugPlayerList();
        sendPlayerStatusUpdate();

        activeStage = activeStage.getNextStage();
        if (checkGameOver() || activeStage == null) return; //Game complete?
        activeStage.onStageStart();
    }

    private void sendPlayerStatusUpdate() {
        try {
            for (Player player : players) {
                JSONObject statusMsg = new JSONObject();
                statusMsg.put("id", PACKET_SYS_PLAYER_STATUS);

                JSONArray items = new JSONArray();
                for (Player opponent : players) {
                    if (opponent == player) {
                        items.put(createPlayerInfoRow(player, true));

                    } else items.put(createPlayerInfoRow(opponent, false));
                }
                statusMsg.put("options", items);

                player.getClient().sendMessage(statusMsg.toString());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private JSONArray createPlayerInfoRow(Player player, boolean showRole) {
        JSONArray info = new JSONArray();
        info.put(player.getUsername());

        boolean isAlive = player.isAlive();
        info.put(isAlive);

        if (player.getRole() == null || (isAlive && !showRole)) {
            info.put("-");

        } else info.put(player.getRole().getName());

        return info;
    }

    public void debugPlayerList() {
        for (Player player : players) {
            if (player.getRole() != null)
                Log.d(MainActivity.TAG, player.getUsername() + " is alive( + " + Boolean.toString(player.isAlive()) + ") is marked(" + Boolean.toString(player.isMarkedForDeath()) + ") role: " + player.getRole().getName());
        }
    }

    private boolean checkGameOver() {
        if (!rolesDistributed) return false;

        ArrayList<Player> werewolves = new ArrayList<>();
        ArrayList<Player> civvies = new ArrayList<>();

        for (Player player : players) {
            if (player.isAlive()) {
                if (player.getRole().getName().equals(GenericRole.roleNameWerewolf)) {
                    werewolves.add(player);
                } else {
                    civvies.add(player);
                }
            }
        }

        JSONObject victoryMessage = new JSONObject();
        try {
            victoryMessage.put("id", PACKET_INFO);

            if (werewolves.size() >= civvies.size()) {
                Log.d(MainActivity.TAG, "[Controller](checkGameOver): Werewolves won");
                victoryMessage.put("phrase", "Werewolves won");
                announce(victoryMessage.toString());
                gameActive = false;
                return true;
            } else if (werewolves.size() == 0) {
                Log.d(MainActivity.TAG, "[Controller](checkGameOver): Civilians won");
                victoryMessage.put("phrase", "Civilians won");
                announce(victoryMessage.toString());
                gameActive = false;
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return false;
    }

    private Player findPlayer(Client client) {
        for (Player player : players) {
            if (player.getClient().equals(client)) return player;
        }
        return null;
    }

    public void killPlayer(Player player) {
        player.setAlive(false);
        alivePlayerCount--;
    }

    public ArrayList<String> convertPlayerArrayToNames(ArrayList<Player> playerArrayList) {
        ArrayList<String> names = new ArrayList<>();
        for (Player player : playerArrayList) names.add(player.getUsername());
        return names;
    }

    public ArrayList<Player> getAlivePlayers() {
        ArrayList<Player> alivePlayers = new ArrayList<>();
        for (Player player : players) {
            if (player.isAlive()) alivePlayers.add(player);
        }

        return alivePlayers;
    }

    public Player findPlayer(String username) {
        for (Player player : players) {
            if (player.getUsername().equals(username)) return player;
        }
        return null;
    }

    public ArrayList<Player> findPlayersByRole(String roleName) {
        ArrayList<Player> playersByRole = new ArrayList<>();
        for (Player player : players) {
            if (player.getRole().getName().equals(roleName)) {
                playersByRole.add(player);
            }
        }
        return playersByRole;
    }

    public void announce(String message) {
        Log.d(MainActivity.TAG, "[Controller](announce): Announcing: " + message);
        for (Player player : players) {
            player.getClient().sendMessage(message);
        }
    }

    public void announceIfReady(String message) {
        for (Player player : players) {
            Log.d(MainActivity.TAG, "Player: " + player.getUsername() + " r: " + Boolean.toString(player.ready));
            if (player.ready) player.getClient().sendMessage(message);
        }
    }

    public void announceAudioMessagePrivate(Player player, String audioFileName) {
        if (audioFileName == null) return;

        JSONObject audioMessage = new JSONObject();
        try {
            audioMessage.put("id", PACKET_AUDIO);
            audioMessage.put("phrase", "audio/sample.mp3");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String msg = audioMessage.toString();

        player.getClient().sendMessage(msg);
    }

    public void announceAudioMessage(String audioFileName) {
        for (Player player : players) {
            if (player.isAlive()) {
                announceAudioMessagePrivate(player, audioFileName);
            }
        }
    }

    public int getCountPlayersReady() {
        int count = 0;
        for (Player player : players) {
            if (player.isAlive()) {
                if (player.ready) count++;
            }
        }
        return count;
    }
}
