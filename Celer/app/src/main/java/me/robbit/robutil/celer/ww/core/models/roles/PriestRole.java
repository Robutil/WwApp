package me.robbit.robutil.celer.ww.core.models.roles;

public class PriestRole extends WitchRole {
    public PriestRole() {
        enablePriest();
    }

    @Override
    public String getName() {
        return GenericRole.roleNamePriest;
    }
}
