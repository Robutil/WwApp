package me.robbit.robutil.celer.ww.activities.server;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

import me.robbit.robutil.celer.MainActivity;
import me.robbit.robutil.celer.R;
import me.robbit.robutil.celer.ww.core.GameModeFactory;
import me.robbit.robutil.celer.ww.core.models.roles.GenericRole;
import me.robbit.robutil.celer.ww.core.models.stages.GenericStage;

public class WwConfigServerActivity extends AppCompatActivity {

    //Ui elements
    private TextView seekBarPlayerCountText;
    private Spinner gameModeSpinner;
    private SeekBar seekBarPlayerCount;
    private TextView seekBarDayDurationText;
    private SeekBar seekBarDayDuration;
    private Switch activateBridge;
    private Button createGameButton;
    private Button configRolesButton;

    //Day duration time in blocks of 10 seconds
    private int minDayDuration = 1;
    private int maxDayDuration = 90;
    private int currentDayDuration = 1;

    //Role config
    //TODO: get preset roles via RoleFactory
    private String[] roles = {"wolf", "witch", "civilian", "slut", "nobleman"};
    public static final String INTENT_ROLES = "current_roles";
    public static final String INTENT_BRIDGE = "bridge";
    public static final String INTENT_PLAYERS = "players";
    public static final String INTENT_DAY_DURATION = "day_duration_in_seconds";
    public static final String INTENT_GAME_MODE = "game_mode";

    //Game mode config
    private ArrayAdapter<String> spinnerAdapter;
    private String selectedGameMode;
    private String[] gameModeNames;

    //Number of players
    private int maxPlayers = 20;
    private int minPlayers = roles.length;
    private int playerCount = minPlayers;

    //Callback
    private boolean bluetoothEnabled = false;
    private final int INTENT_BLUETOOTH = 1;
    private final int INTENT_ROLES_CONFIGURE = 2;
    private final int INTENT_BLUETOOTH_ACTIVATED = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ww_config_server);

        initPlayerCountSeekBar();
        initGameModeSpinner();
        initBridgeSwitch();
        initCreateGameButton();
        initConfigRolesButton();
        initDaySeekBar();
    }

    private void initGameModeSpinner() {
        gameModeSpinner = (Spinner) findViewById(R.id.WwConfigServerActivitySpinner);

        GameModeFactory gameModeFactory = new GameModeFactory();
        gameModeNames = gameModeFactory.getGameModeNames();

        spinnerAdapter = new ArrayAdapter<>(WwConfigServerActivity.this, R.layout.ww_config_spinner_item, gameModeNames);
        spinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        gameModeSpinner.setAdapter(spinnerAdapter);

        selectedGameMode = (String) gameModeSpinner.getSelectedItem();

        gameModeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedGameMode = (String) gameModeSpinner.getSelectedItem();
                if (!selectedGameMode.equals(GameModeFactory.getGameModeName(GameModeFactory.GAME_MODE_ARYE))) {
                    updateSeekBarSafe(roles.length);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initPlayerCountSeekBar() {
        seekBarPlayerCount = (SeekBar) findViewById(R.id.WwActivitySeekbar);
        seekBarPlayerCountText = (TextView) findViewById(R.id.WwActivitySeekbarText);

        seekBarPlayerCountText.setText(String.format(getString(R.string.space), minPlayers));
        seekBarPlayerCount.setMax(maxPlayers - minPlayers);

        seekBarPlayerCount.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updatePlayerCountSeekBar(progress);

                if (!selectedGameMode.equals(GameModeFactory.getGameModeName(GameModeFactory.GAME_MODE_ARYE))) {
                    if (playerCount > roles.length) {
                        String[] randomExtraRoles = new String[playerCount];
                        System.arraycopy(roles, 0, randomExtraRoles, 0, roles.length);
                        for (int i = 0; i < (playerCount - roles.length); i++) {
                            randomExtraRoles[roles.length + i] = GenericRole.roleNameCivilian;
                        }

                        roles = randomExtraRoles;

                    } else if (playerCount < roles.length) {
                        String[] newRoleList = new String[playerCount];
                        System.arraycopy(roles, 0, newRoleList, 0, playerCount);
                        roles = newRoleList;
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //Stub
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //Stub
            }
        });
    }

    private void updatePlayerCountSeekBar(int progress) {
        playerCount = minPlayers + progress;
        //// FIXME: 1-8-17 : I am not sure whether the training space is better than a trailing zero
//        if (playerCount < 10) {
//            seekBarPlayerCountText.setText(String.format(getString(R.string.space), playerCount));
//        } else {
        seekBarPlayerCountText.setText(String.format(Locale.ENGLISH, "%02d", playerCount));
//        }
    }

    private void initDaySeekBar() {
        seekBarDayDuration = (SeekBar) findViewById(R.id.WwConfigServerActivitySeekBarDay);
        seekBarDayDurationText = (TextView) findViewById(R.id.WwConfigServerActivitySeekBarDayText);

        seekBarDayDuration.setMax(maxDayDuration - minDayDuration);
        seekBarDayDuration.setProgress(currentDayDuration - minDayDuration);
        updateDayDurationSeekBar(currentDayDuration - minDayDuration);
        seekBarDayDuration.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateDayDurationSeekBar(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void updateDayDurationSeekBar(int progress) {
        currentDayDuration = minDayDuration + progress;

        int totalSeconds = currentDayDuration * 10;
        int minutes = totalSeconds / 60;
        int seconds = totalSeconds % 60;

        String text = String.format(Locale.ENGLISH, "%02d:%02d", minutes, seconds);
        seekBarDayDurationText.setText(text);
    }

    private void initBridgeSwitch() {
        activateBridge = (Switch) findViewById(R.id.WwActivityBridgeEnable);

        activateBridge.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
                        Intent enableBluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBluetoothIntent, INTENT_BLUETOOTH);

                    } else {
                        bluetoothEnabled = true;
                        sendBluetoothBridgeInformation();
                    }

                } else {
                    bluetoothEnabled = false;
                }
            }
        });
    }

    private void initCreateGameButton() {
        createGameButton = (Button) findViewById(R.id.WwActivityButton);

        createGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playerCount > roles.length) {
                    Toast.makeText(WwConfigServerActivity.this, "Configure more roles!", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent startWwServerIntent = new Intent(getApplicationContext(), WwServerActivity.class);

                startWwServerIntent.putExtra(INTENT_BRIDGE, bluetoothEnabled);
                startWwServerIntent.putExtra(INTENT_PLAYERS, playerCount);
                startWwServerIntent.putExtra(INTENT_ROLES, roles);
                startWwServerIntent.putExtra(INTENT_DAY_DURATION, currentDayDuration * 10);
                startWwServerIntent.putExtra(INTENT_GAME_MODE, selectedGameMode);

                startActivity(startWwServerIntent);
            }
        });
    }

    private void initConfigRolesButton() {
        configRolesButton = (Button) findViewById(R.id.WwConfigActivityConfigureRoles);

        configRolesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startWwServerIntent = new Intent(getApplicationContext(), WwConfigRolesActivity.class);
                startWwServerIntent.putExtra(INTENT_ROLES, roles);
                startWwServerIntent.putExtra(INTENT_GAME_MODE, selectedGameMode);

                startActivityForResult(startWwServerIntent, INTENT_ROLES_CONFIGURE);
            }
        });
    }

    private void updateSeekBarSafe(int progress) {
        if (progress > 0) {
            if (progress < minPlayers) minPlayers = progress;
            if (progress > maxPlayers) maxPlayers = progress;
        }
        seekBarPlayerCount.setMax(maxPlayers - minPlayers);
        seekBarPlayerCount.setProgress(progress - minPlayers);
        updatePlayerCountSeekBar(progress - minPlayers);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == INTENT_BLUETOOTH) {
            if (resultCode == INTENT_BLUETOOTH_ACTIVATED) {
                bluetoothEnabled = true;
                sendBluetoothBridgeInformation();
            } else {
                activateBridge.setChecked(false);
            }
        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == INTENT_ROLES_CONFIGURE) {
                roles = data.getStringArrayExtra(INTENT_ROLES);

                if (selectedGameMode.equals(GameModeFactory.getGameModeName(GameModeFactory.GAME_MODE_VANILLA))) {
                    int amountOfPlayers = roles.length;
                    if (amountOfPlayers > 0) {
                        if (amountOfPlayers < minPlayers) minPlayers = amountOfPlayers;
                        if (amountOfPlayers > maxPlayers) maxPlayers = amountOfPlayers;
                    }
                    seekBarPlayerCount.setMax(maxPlayers - minPlayers);
                    seekBarPlayerCount.setProgress(amountOfPlayers - minPlayers);
                    updatePlayerCountSeekBar(amountOfPlayers - minPlayers);
                }
            }
        }
    }

    void sendBluetoothBridgeInformation() {
        String msg = "Other devices can connect when the game has been created";
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
