package me.robbit.robutil.celer.ww.activities.server;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import me.robbit.robutil.celer.R;
import me.robbit.robutil.celer.ww.core.GameModeFactory;
import me.robbit.robutil.celer.ww.core.models.roles.RoleFactory;

public class WwConfigAddRoleActivity extends AppCompatActivity {

    public static final String INTENT_CHOSEN_ROLE = "chosen_role";

    private Spinner spinner;
    private TextView description;
    private HashMap<String, String> rolesWithDescriptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ww_config_add_role);

        Intent intent = getIntent();
        String gameModeName = intent.getStringExtra(WwConfigServerActivity.INTENT_GAME_MODE);
        GameModeFactory gameModeFactory = new GameModeFactory();
        int gameMode = gameModeFactory.getGameModeIndex(gameModeName);

        spinner = (Spinner) findViewById(R.id.WwConfigAddRoleActivitySpinner);
        description = (TextView) findViewById(R.id.WwConfigAddRoleActivityDescription);
        Button saveButton = (Button) findViewById(R.id.WwConfigAddRoleActivityButton);

        RoleFactory roleFactory = new RoleFactory();
        ArrayList<String> roles = roleFactory.getRoleList(gameMode);
        rolesWithDescriptions = roleFactory.matchRolesWithDescriptions(roles);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.ww_config_spinner_item, roles);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        updateDescription();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateDescription();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                updateDescription();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra(INTENT_CHOSEN_ROLE, (String) spinner.getSelectedItem());
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        });
    }

    private void updateDescription() {
        String key = (String) spinner.getSelectedItem();
        String value = rolesWithDescriptions.get(key);
        if (value != null) description.setText(value);
        else description.setText("");
    }
}
