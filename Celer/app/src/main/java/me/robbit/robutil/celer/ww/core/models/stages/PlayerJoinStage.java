package me.robbit.robutil.celer.ww.core.models.stages;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import me.robbit.robutil.celer.MainActivity;
import me.robbit.robutil.celer.ww.core.Controller;
import me.robbit.robutil.celer.ww.core.models.Player;

public class PlayerJoinStage extends GenericStage {

    public PlayerJoinStage(Controller controller) {
        super(controller);
    }

    @Override
    public void onMessage(Player player, String message) {
        try {
            JSONObject msg = new JSONObject(message);
            int packetId = msg.getInt("id");
            Log.d(MainActivity.TAG, "Got new message, id: " + packetId);

            if (packetId == controller.PACKET_USERNAME) handlePacketUsername(player, msg);

        } catch (JSONException e) {
            Log.d(MainActivity.TAG, "Controller: could not parse msg: " + e.toString());
        }
    }

    @Override
    public GenericStage getNextStage() {
        return new RoleStage(controller);
    }

    @Override
    public int getStageId() {
        return STAGE_PLAYER_JOIN;
    }

    @Override
    public void run() {
        //Stub
    }

    private void handlePacketUsername(Player player, JSONObject packet) throws JSONException {
        String username = packet.getString("msg");
        if (player.getUsername() == null) {
            player.setUsername(username);
            controller.currentNumberOfPlayers++;

            sendPacketAnnouncePlayerJoined(username);

            //All players arrived, goto next stage
            if (controller.currentNumberOfPlayers == controller.alivePlayerCount) {
                controller.activateNextStage();
            }
        }
    }

    private void sendPacketAnnouncePlayerJoined(String username) throws JSONException {
        JSONObject packet = new JSONObject();
        packet.put("id", controller.PACKET_WAIT);
        packet.put("msg", username);
        packet.put("current", controller.currentNumberOfPlayers);
        packet.put("max", controller.alivePlayerCount);

        controller.announce(packet.toString());
    }
}
