package me.robbit.robutil.celer.ww.bridge;

import me.robbit.robutil.celer.ww.core.Client;

public class FakeClient implements Client {

    private final String name;
    private BridgeServer callback;

    public FakeClient(BridgeServer callback, String name) {
        this.name = name;
        this.callback = callback;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void sendMessage(String message) {
        callback.sendMessage(this, message);
    }
}
