package me.robbit.robutil.celer.ww.core;

public interface Client{

    String getName();

    void sendMessage(String message);

}
