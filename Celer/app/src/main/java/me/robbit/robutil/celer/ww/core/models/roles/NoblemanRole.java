package me.robbit.robutil.celer.ww.core.models.roles;

import java.util.ArrayList;
import java.util.Random;

import me.robbit.robutil.celer.ww.core.models.Player;

public class NoblemanRole extends GenericRole {

    private ArrayList<Player> servants = null;

    @Override
    public String getName() {
        return GenericRole.roleNameNobleman;
    }

    public ArrayList<Player> getServants() {
        if (servants != null) return servants;
        servants = new ArrayList<>();

        //TODO: make this a utility function in controller
        ArrayList<Player> civilPlayers = new ArrayList<>();
        for (Player player : controller.players) {
            if (!player.getRole().getName().equals(GenericRole.roleNameWerewolf)) {

                //Cannot be own servant
                if (player != playerCallback) civilPlayers.add(player);
            }
        }

        //TODO: make this a utility function
        //Pick two random players
        Random random = new Random();
        for (int i = 0; i < 2; i++) {
            servants.add(civilPlayers.remove(random.nextInt(civilPlayers.size())));
        }

        return servants;
    }
}
