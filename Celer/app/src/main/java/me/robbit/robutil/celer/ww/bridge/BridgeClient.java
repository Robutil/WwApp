package me.robbit.robutil.celer.ww.bridge;

import android.bluetooth.BluetoothDevice;

import java.util.Set;

import me.robbit.robutil.celer.ww.core.Client;
import me.robbit.robutil.celer.ww.core.GenericController;
import robbit.bluetooth.bridge.BluetoothBridgeCallback;
import robbit.bluetooth.bridge.BluetoothBridgeClient;
import robbit.net.protocol.Packet;

public class BridgeClient implements BluetoothBridgeCallback, Client {

    private GenericController controller;

    private BluetoothBridgeClient peer;

    public BridgeClient() {
        peer = new BluetoothBridgeClient();
    }

    public Set<BluetoothDevice> getBondedDevices() {
        return peer.getBondedDevices();
    }

    public void start(BluetoothDevice device, GenericController controller) {
        this.controller = controller;
        peer.connect(device, this);
    }

    @Override
    public void onOpen() {
        controller.onOpen(this);
    }

    @Override
    public void onError(Exception ex) {
        controller.onError(this, ex);
    }

    @Override
    public void onPacket(Packet packet) {
        controller.onMessage(this, packet.toString());
    }

    @Override
    public void onClose(Exception ex) {
        controller.onClose(this);
    }

    @Override
    public String getName() {
        return "peer";
    }

    @Override
    public void sendMessage(String message) {
        peer.sendPacket(new Packet(message));
    }

    public void stop() {
        peer.stop();
    }
}
