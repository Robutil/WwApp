package me.robbit.robutil.celer.ww.core.models.roles;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import me.robbit.robutil.celer.MainActivity;
import me.robbit.robutil.celer.ww.core.GameModeFactory;
import me.robbit.robutil.celer.ww.core.models.stages.GenericStage;

import static me.robbit.robutil.celer.ww.core.GameModeFactory.GAME_MODE_ARYE;
import static me.robbit.robutil.celer.ww.core.GameModeFactory.GAME_MODE_VANILLA;

public class RoleFactory {

    private final int gameMode;
    private int numberOfPlayers;

    private Random random = new Random();
    private ArrayList<GenericRole> roles = null;

    public RoleFactory() {
        gameMode = GAME_MODE_VANILLA;
    }

    public RoleFactory(int gameMode) {
        this.gameMode = gameMode;
    }

    public RoleFactory(int gameMode, int numberOfPlayers, String[] rolesByName) {
        this.gameMode = gameMode;
        this.numberOfPlayers = numberOfPlayers;

        generateRoles(rolesByName);
    }

    private void generateRoles(String[] rolesByName) {
        switch (gameMode) {
            case GAME_MODE_VANILLA:
                Log.d(MainActivity.TAG, "[RoleFactory](generateRoles): Generating roles for vanilla");
                generateRolesFromNames(rolesByName);
                break;

            case GAME_MODE_ARYE:
                Log.d(MainActivity.TAG, "[RoleFactory](generateRoles): Generating roles for ARYE");
                generateRolesArye(rolesByName, numberOfPlayers);
                break;

            default:
                break;
        }
    }

    public ArrayList<String> getRoleList() {
        return getRoleList(GameModeFactory.GAME_MODE_VANILLA);
    }

    public ArrayList<String> getRoleList(int gameMode) {
        ArrayList<String> roleList = new ArrayList<>();
        roleList.add(GenericRole.roleNameCivilian);
        roleList.add(GenericRole.roleNameSlut);
        roleList.add(GenericRole.roleNameWerewolf);
        roleList.add(GenericRole.roleNameWitch);
        roleList.add(GenericRole.roleNameTwin);
        roleList.add(GenericRole.roleNameNobleman);
        roleList.add(GenericRole.roleNameWitchApprentice);
        roleList.add(GenericRole.roleNamePriest);

        if (gameMode == GameModeFactory.GAME_MODE_ARYE) {
            roleList.add(GenericRole.roleNameDrunkBartender);
        }

        return roleList;
    }

    public HashMap<String, String> matchRolesWithDescriptions(ArrayList<String> roles) {
        HashMap<String, String> rolesWithDescriptions = new HashMap<>();

        for (String role : roles) {
            rolesWithDescriptions.put(role, getRoleDescription(role));
        }

        return rolesWithDescriptions;
    }

    //TODO: These lists must be optimized, this is madness
    public String getRoleDescription(String role) {
        if (role.equals(GenericRole.roleNameCivilian))
            return GenericRole.roleDescriptionCivilian;

        else if (role.equals(GenericRole.roleNameWerewolf))
            return GenericRole.roleDescriptionWerewolf;

        else if (role.equals(GenericRole.roleNameWitch))
            return GenericRole.roleDescriptionWitch;

        else if (role.equals(GenericRole.roleNameSlut))
            return GenericRole.roleDescriptionSlut;

        else if (role.equals(GenericRole.roleNameTwin))
            return GenericRole.roleDescriptionTwin;

        else if (role.equals(GenericRole.roleNameNobleman))
            return GenericRole.roleDescriptionNobleman;

        else if (role.equals(GenericRole.roleNameWitchApprentice))
            return GenericRole.roleDescriptionWitchApprentice;

        else if (role.equals(GenericRole.roleNamePriest))
            return GenericRole.roleDescriptionPriest;

        else if (role.equals(GenericRole.roleNameDrunkBartender))
            return GenericRole.roleDescriptionDrunkBartender;

        return null;
    }

    public GenericRole getRoleByName(String role) {
        if (role.equals(GenericRole.roleNameCivilian))
            return new CivilianRole();

        else if (role.equals(GenericRole.roleNameWerewolf))
            return new WerewolfRole();

        else if (role.equals(GenericRole.roleNameWitch))
            return new WitchRole();

        else if (role.equals(GenericRole.roleNameSlut))
            return new SlutRole();

        else if (role.equals(GenericRole.roleNameTwin))
            return new TwinRole();

        else if (role.equals(GenericRole.roleNameNobleman))
            return new NoblemanRole();

        else if (role.equals(GenericRole.roleNameWitchApprentice))
            return new WitchApprenticeRole();

        else if (role.equals(GenericRole.roleNamePriest))
            return new PriestRole();

        else if (role.equals(GenericRole.roleNameDrunkBartender))
            return new DrunkBartenderRole();

        else
            return null;
    }

    public ArrayList<GenericRole> generateRolesFromNames(String[] rolesByName) {
        roles = new ArrayList<>();

        for (String roleName : rolesByName) roles.add(getRoleByName(roleName));
        Log.d(MainActivity.TAG, roles.toString());
        return roles;
    }

    private ArrayList<GenericRole> generateRolesVanilla() {
        roles = new ArrayList<>();

        //TODO: this stuff is bugged, somewhere

        if (numberOfPlayers == 6) {
            roles.add(new WerewolfRole());
            roles.add(new WitchRole());
            roles.add(new SlutRole());
            for (int i = 0; i < 3; i++) {
                roles.add(new CivilianRole());
            }
        } else if (numberOfPlayers == 7) {
            roles.add(new WerewolfRole());
            for (int i = 0; i < 6; i++) {
                roles.add(new CivilianRole());
            }
        } else {
            roles.add(new WerewolfRole());
            roles.add(new WerewolfRole());

            for (int i = 0; i < numberOfPlayers - 2; i++) {
                roles.add(new CivilianRole());
            }
        }

        if (roles.size() == 0) return null;
        else return roles;
    }

    public void generateRolesArye(String[] rolesByName, int playerCount) {
        ArrayList<GenericRole> allPossibleRoles = generateRolesFromNames(rolesByName);
        ArrayList<GenericRole> randomRolePool = new ArrayList<>();
        roles = new ArrayList<>();

        int mustHaveRoleCounter = 0;
        for (GenericRole role : allPossibleRoles) {
            if (role.getName().equals(GenericRole.roleNameWerewolf) || role.getName().equals(GenericRole.roleNameDrunkBartender)) {
                //Must have roles
                roles.add(role);
                mustHaveRoleCounter++;
            } else {
                //Role can be picked randomly from pool
                randomRolePool.add(role);
            }
        }

        for (int i = 0; i < playerCount - mustHaveRoleCounter; i++) {
            roles.add(getNextRandomRole(randomRolePool));
        }
    }

    public GenericRole getNextRandomRole(ArrayList<GenericRole> pool) {
        return pool.remove(random.nextInt(pool.size()));
    }

    public GenericRole getNextRandomRole() {
        return getNextRandomRole(roles);
    }

}
