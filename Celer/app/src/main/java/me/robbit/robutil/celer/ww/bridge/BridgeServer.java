package me.robbit.robutil.celer.ww.bridge;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.robbit.robutil.celer.MainActivity;
import me.robbit.robutil.celer.ww.core.Client;
import me.robbit.robutil.celer.ww.core.GenericController;
import robbit.bluetooth.bridge.BluetoothBridgeCallback;
import robbit.bluetooth.bridge.BluetoothBridgeServer;
import robbit.net.protocol.Packet;

public class BridgeServer implements BluetoothBridgeCallback {

    private GenericController controller;
    private BluetoothBridgeServer peer;

    private ArrayList<Client> clients = new ArrayList<>();

    public static final int CLIENT_CONNECT = 1;
    public static final int CLIENT_CLOSED = 2;
    public static final int CLIENT_ERROR = 3;
    public static final int CLIENT_MESSAGE = 4;

    public BridgeServer() {
        peer = new BluetoothBridgeServer();
    }

    public void start(GenericController controller) {
        this.controller = controller;
        peer.start(this);
    }

    /**
     * Is called when the Bluetooth Bridge is opened. Not related to any clients, though.
     */
    @Override
    public void onOpen() {
        //Stub
    }

    @Override
    public void onClose(Exception ex) {
        for (Client client : clients) {
            controller.onClose(client);
        }
    }

    @Override
    public void onError(Exception ex) {
        for (Client client : clients) {
            controller.onError(client, ex);
        }
    }

    @Override
    public void onPacket(Packet packet) {
        String payload = packet.toString();

        try {
            JSONObject message = new JSONObject(payload);
            int packetId = message.getInt("id");

            switch (packetId) {
                case CLIENT_CONNECT:
                    handleClientConnect(message);
                    break;
                case CLIENT_ERROR:
                case CLIENT_CLOSED:
                    handleClientClosed(message);
                    break;
                case CLIENT_MESSAGE:
                    handleClientMessage(message);
                    break;
                default:
                    //Stub
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleClientConnect(JSONObject message) throws JSONException {
        String origin = message.getString("origin");
        Client client = new FakeClient(this, origin);

        clients.add(client);
        controller.onOpen(client);
    }

    private void handleClientClosed(JSONObject message) throws JSONException {
        String origin = message.getString("origin");
        Client client = findClientByName(origin);
        if (client != null) {
            controller.onClose(client);

        } else {
            Log.d(MainActivity.TAG, "BRIDGE_S: ClientClosed: Unknown client");
        }
    }

    private void handleClientMessage(JSONObject message) throws JSONException {
        String origin = message.getString("origin");
        String msg = message.getString("msg");

        Client client = findClientByName(origin);
        if (client != null) {
            controller.onMessage(client, msg);

        } else {
            Log.d(MainActivity.TAG, "BRIDGE_S: ClientMessage: Unknown client");
        }
    }

    public void sendMessage(Client client, String message) {
        try {
            JSONObject wrapper = new JSONObject();
            wrapper.put("id", CLIENT_MESSAGE);
            wrapper.put("target", client.getName());
            wrapper.put("msg", message);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private Client findClientByName(String name) {
        for (Client client : clients) {
            if (client.getName().equals(name)) return client;
        }
        return null;
    }

    public void stop() {
        peer.stop();
    }
}
