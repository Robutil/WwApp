package me.robbit.robutil.celer.ww.core.models.stages;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.robbit.robutil.celer.MainActivity;
import me.robbit.robutil.celer.ww.core.Controller;
import me.robbit.robutil.celer.ww.core.models.Player;
import me.robbit.robutil.celer.ww.core.models.roles.GenericRole;
import me.robbit.robutil.celer.ww.core.models.roles.SlutRole;

public class NightResultStage extends GenericStage {

    public NightResultStage(Controller controller) {
        super(controller);
    }

    @Override
    public void onMessage(Player player, String message) {
        try {
            Log.d(MainActivity.TAG, "[NightResultStage](onMessage): " + player.getUsername());
            Log.d(MainActivity.TAG, "[NightResultStage](onMessage): " + Integer.toString(controller.getCountPlayersReady()) + "/" + Integer.toString(controller.alivePlayerCount));

            JSONObject msg = new JSONObject(message);
            int packetId = msg.getInt("id");

            if (packetId == controller.PACKET_GENERIC && player.isAlive())
                handleInfoSeen(player, msg);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void handleInfoSeen(Player player, JSONObject msg) {
        player.ready = true;

        JSONObject announce = generateWaitPacket(player.getUsername(), controller.getCountPlayersReady(), controller.alivePlayerCount);
        controller.announceIfReady(announce.toString());

        if (controller.getCountPlayersReady() == controller.alivePlayerCount) {
            controller.activateNextStage();
        }
    }

    @Override
    public GenericStage getNextStage() {
        return new DayStage(controller);
    }

    @Override
    public int getStageId() {
        return GenericStage.STAGE_NIGHT_RESULT;
    }

    @Override
    public void run() {
        ArrayList<Player> newDeadPlayers = new ArrayList<>();
        for (Player player : controller.players) {
            if (player.isMarkedForDeath() && player.isAlive()) {
                newDeadPlayers.add(player);
                Log.d(MainActivity.TAG, player.getUsername() + " died. Role: " + player.getRole().getName());
            }
        }

        checkAndHandleIfSlutDied(newDeadPlayers);

        for (Player player : newDeadPlayers) {
            controller.killPlayer(player);
        }

        JSONObject msg = new JSONObject();
        try {
            msg.put("id", controller.PACKET_INFO);
            msg.put("phrase", "Dead players");
            msg.put("options", createNeatInfoOptions(newDeadPlayers));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        controller.announce(msg.toString());
    }

    private JSONArray createNeatInfoOptions(ArrayList<Player> deadPlayers) {
        //Requires separate array list due to JSON implementation...
        ArrayList<String> neatOpts = new ArrayList<>();

        for (Player player : deadPlayers) {
            neatOpts.add(player.getUsername() + ", role: " + player.getRole().getName());
        }

        return new JSONArray(neatOpts);
    }

    private void checkAndHandleIfSlutDied(ArrayList<Player> newDeadPlayers) {
        ArrayList<Player> sluts = controller.findPlayersByRole(GenericRole.roleNameSlut);

        for (Player player : sluts) {
            if (player.isAlive()) {
                SlutRole slutRole = (SlutRole) player.getRole();

                if (newDeadPlayers.contains(slutRole.getTargetPlayer())) {
                    //Check if Slut dies, because her target dies
                    newDeadPlayers.add(player);

                } else if (newDeadPlayers.contains(player)) {
                    //Check if slut survives, because her target survives
                    newDeadPlayers.remove(player);
                }
            }
        }
    }
}
