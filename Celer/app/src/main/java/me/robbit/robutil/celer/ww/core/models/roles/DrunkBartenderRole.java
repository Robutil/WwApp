package me.robbit.robutil.celer.ww.core.models.roles;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.robbit.robutil.celer.ww.core.models.Player;
import me.robbit.robutil.celer.ww.core.models.stages.GenericStage;

public class DrunkBartenderRole extends GenericRole {

    private ArrayList<GenericRole> rolesNotKnown = null;
    private RoleFactory roleFactory = new RoleFactory();

    private void init() {
        rolesNotKnown = new ArrayList<>();
        for (Player player : controller.players) {
            rolesNotKnown.add(player.getRole());
        }
    }

    @Override
    public String getName() {
        return GenericRole.roleNameDrunkBartender;
    }

    @Override
    public void handelStageStart(int stageId) {
        if (stageId == GenericStage.STAGE_NIGHT_ONE) {
            handleStageNightOne();
        } else super.handelStageStart(stageId);
    }

    private void handleStageNightOne() {
        if (rolesNotKnown == null) init();

        JSONObject msg = new JSONObject();
        try {
            msg.put("id", controller.PACKET_INFO);
            msg.put("phrase", "The following roles are in play");

            ArrayList<String> publishRole = new ArrayList<>();
            if (rolesNotKnown.size() > 2) {
                for (int i = 0; i < 2; i++) {
                    publishRole.add(roleFactory.getNextRandomRole(rolesNotKnown).getName());
                }
            } else {
                for (GenericRole role : rolesNotKnown) {
                    publishRole.add(role.getName());
                }
            }

            msg.put("options", new JSONArray(publishRole));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        playerCallback.getClient().sendMessage(msg.toString());
    }

    @Override
    public void handleMessage(int stageId, JSONObject msg, Player origin) throws JSONException {
        if (stageId == GenericStage.STAGE_NIGHT_ONE) {
            playerCallback.ready = true;

        } else super.handleMessage(stageId, msg, origin);
    }
}
