package me.robbit.robutil.celer.ww.core.models.roles;

public class WitchApprenticeRole extends WitchRole {

    public WitchApprenticeRole(){
        enableApprentice();
    }

    @Override
    public String getName() {
        return GenericRole.roleNameWitchApprentice;
    }

    //BUGGED
}
