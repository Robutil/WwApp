package me.robbit.robutil.celer.ww.core.models.stages;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import me.robbit.robutil.celer.MainActivity;
import me.robbit.robutil.celer.ww.core.Controller;
import me.robbit.robutil.celer.ww.core.models.Player;

public class DayStage extends GenericStage {

    public DayStage(Controller controller) {
        super(controller);
    }

    @Override
    public void onMessage(Player player, String message) {
        player.ready = true;

        if (controller.getCountPlayersReady() == controller.alivePlayerCount) {
            controller.activateNextStage();
        }
    }

    @Override
    public GenericStage getNextStage() {
        return new VoteStage(controller);
    }

    @Override
    public int getStageId() {
        return GenericStage.STAGE_DAY;
    }

    @Override
    public void run() {
        try {
            JSONObject announce = generateWaitPacket("Day", 0, controller.dayDurationInSeconds, "time");
            announce.put("skip", true);

            controller.announce(announce.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
