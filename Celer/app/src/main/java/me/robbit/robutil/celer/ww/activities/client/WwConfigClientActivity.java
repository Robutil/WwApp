package me.robbit.robutil.celer.ww.activities.client;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Set;

import me.robbit.robutil.celer.MainActivity;
import me.robbit.robutil.celer.R;
import me.robbit.robutil.celer.ww.bridge.BridgeClient;

public class WwConfigClientActivity extends AppCompatActivity implements Runnable {

    private BluetoothAdapter bluetoothAdapter;
    private BridgeClient peer;

    private ArrayAdapter<String> adapter;
    Set<BluetoothDevice> bondedDevices;
    ArrayList<BluetoothDevice> scannedDevices;
    ArrayList<String> suitableDevices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ww_config_client);

        ListView listView = (ListView) findViewById(R.id.WwActivityClientList);

        peer = new BridgeClient();

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(broadcastReceiver, filter);

        suitableDevices = new ArrayList<>();
        scannedDevices = new ArrayList<>();
        bondedDevices = peer.getBondedDevices();
        adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, suitableDevices);
        listView.setAdapter(adapter);

        final Thread continueScanning = new Thread(this);
        new Thread(this).start();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                continueScanning.interrupt();
                if (bluetoothAdapter.isDiscovering()) bluetoothAdapter.cancelDiscovery();

                String clickedDevice = suitableDevices.get(position);
                Intent intent = new Intent(getApplicationContext(), WwClientActivity.class);

                for (BluetoothDevice device : scannedDevices) {
                    if (device.getName().equals(clickedDevice)) {
                        intent.putExtra("device", device);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                scannedDevices.add(device);
                Log.d(MainActivity.TAG, "Found new device: " + device.getName() + " d: " + BluetoothAdapter.getDefaultAdapter().isDiscovering());

                updateList();
            }
        }
    };

    void updateList() {
        for (BluetoothDevice device : scannedDevices) {
            if (bondedDevices.contains(device)) {
                if (!suitableDevices.contains(device.getName())) {
                    suitableDevices.add(device.getName());
                }
            }
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bluetoothAdapter.cancelDiscovery();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                if (!bluetoothAdapter.isDiscovering()) {
                    bluetoothAdapter.startDiscovery();
                }
                Thread.sleep(5 * 1000);
            }
        } catch (InterruptedException e) {
            bluetoothAdapter.cancelDiscovery();
            Log.d(MainActivity.TAG, "Stopping discovery");
        }
    }
}
