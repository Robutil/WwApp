package me.robbit.robutil.celer.ww.core.models.roles;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.robbit.robutil.celer.ww.core.models.Player;
import me.robbit.robutil.celer.ww.core.models.stages.GenericStage;

public class SlutRole extends GenericRole {

    private Player targetPlayer = null;

    @Override
    public String getName() {
        return GenericRole.roleNameSlut;
    }

    @Override
    public void handelStageStart(int stageId) {
        if (stageId == GenericStage.STAGE_NIGHT_ONE) {
            handleStageNightOne();

        } else super.handelStageStart(stageId);
    }

    private void handleStageNightOne() {
        ArrayList<Player> alivePlayers = controller.getAlivePlayers();
        alivePlayers.remove(playerCallback); //Must sleep with someone
        ArrayList<String> alivePlayerNames = controller.convertPlayerArrayToNames(alivePlayers);

        JSONObject msg = new JSONObject();
        try {
            msg.put("id", controller.PACKET_OPTS);
            msg.put("phrase", "Where do you want to spend the night?");
            msg.put("options", new JSONArray(alivePlayerNames));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        playerCallback.getClient().sendMessage(msg.toString());
    }

    @Override
    public void handleMessage(int stageId, JSONObject msg, Player origin) throws JSONException {
        if (stageId == GenericStage.STAGE_NIGHT_ONE) {
            handleMessageNightOne(msg, origin);

        } else super.handleMessage(stageId, msg, origin);
    }

    private void handleMessageNightOne(JSONObject msg, Player origin) throws JSONException {
        if (msg.getInt("id") == controller.PACKET_GENERIC) {
            String target = msg.getString("answer");
            targetPlayer = controller.findPlayer(target);

            playerCallback.ready = true;
        }
    }

    public Player getTargetPlayer() {
        return targetPlayer;
    }
}
