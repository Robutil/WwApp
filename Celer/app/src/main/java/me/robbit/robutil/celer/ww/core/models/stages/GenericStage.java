package me.robbit.robutil.celer.ww.core.models.stages;

import org.json.JSONException;
import org.json.JSONObject;

import me.robbit.robutil.celer.ww.core.Controller;
import me.robbit.robutil.celer.ww.core.models.Player;

public abstract class GenericStage implements Runnable {

    public final static int STAGE_PLAYER_JOIN = 1;
    public final static int STAGE_ROLE = 2;
    public final static int STAGE_NIGHT_ONE = 3;
    public final static int STAGE_NIGHT_TWO = 4;
    public final static int STAGE_NIGHT_RESULT = 5;
    public final static int STAGE_DAY = 6;
    public final static int STAGE_VOTE = 7;

    protected Controller controller;
    private Thread thread;

    public GenericStage(Controller controller) {
        this.controller = controller;
        thread = new Thread(this);
    }

    public void onStageStart() {
        resetPlayersReady();

        JSONObject msg = new JSONObject();
        try {
            msg.put("id", controller.PACKET_STAGE_ANNOUNCE);
            msg.put("stage", getStageId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        controller.announce(msg.toString());

        thread.run();
    }

    public void invokeRoleStageStart(int stageId) {
        for (Player player : controller.players) {
            player.getRole().handelStageStart(stageId);
        }
    }

    public void resetPlayersReady() {
        for (Player player : controller.players) {
            player.ready = false;
        }
    }

    public JSONObject generateWaitPacket(String message, int cur, int max) {
        return generateWaitPacket(message, cur, max, "default");
    }

    public JSONObject generateWaitPacket(String message, int cur, int max, String type) {
        JSONObject response = new JSONObject();
        try {
            response.put("id", controller.PACKET_WAIT);
            response.put("msg", message);
            response.put("current", cur);
            response.put("max", max);
            response.put("type", type);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return response;
    }

    public void terminateStage() {
        thread.interrupt();
    }

    public abstract void onMessage(Player player, String message);

    public abstract GenericStage getNextStage();

    public abstract int getStageId();
}
