package me.robbit.robutil.celer.ww.core.models.roles;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

import me.robbit.robutil.celer.MainActivity;
import me.robbit.robutil.celer.ww.core.Controller;
import me.robbit.robutil.celer.ww.core.models.Player;
import me.robbit.robutil.celer.ww.core.models.stages.GenericStage;

public abstract class GenericRole {

    public String target = "";

    protected Controller controller;
    protected ArrayList<Player> players;
    protected Player playerCallback;

    private Random random = new Random();
    private final String operators = "+-*:%";

    //For generic option;
    private int genericPhraseAnswer;

    public static String roleNameCivilian = "civilian";
    public static String roleNameSlut = "slut";
    public static String roleNameTwin = "twin";
    public static String roleNameWerewolf = "wolf";
    public static String roleNameWitch = "witch";
    public static String roleNameNobleman = "nobleman";
    public static String roleNameWitchApprentice = "witch_apprentice";
    public static String roleNamePriest = "priest";
    public static String roleNameDrunkBartender = "drunk_bartender";

    public static String roleDescriptionCivilian = "Vote to kill people during the day. No special" +
            " abilities";
    public static String roleDescriptionSlut = "Spend the night with someone else. If someone " +
            "tries to kill you during the night, you are unharmed. However, if the person where you " +
            "are sleeping is killed, you die as well.";
    public static String roleDescriptionTwin = "At the start of the game you get to know who your " +
            "twin counterpart is";
    public static String roleDescriptionWerewolf = "Use your powers to eliminate others in the night";
    public static String roleDescriptionWitch = "Use your powers to save or kill others in the night";
    public static String roleDescriptionNobleman = "You have two civilian servants who are informed " +
            "of your identity. The servants do not know each other identity";
    public static String roleDescriptionWitchApprentice = "Like the witch you have the option to " +
            "save or kill someone in the night. However, since your magical powers are not as " +
            "advanced you cannot use both powers in a single game";
    public static String roleDescriptionPriest = "You have the option to save someone, once, in " +
            "the night";
    public static String roleDescriptionDrunkBartender = "Every night you talk to your customers," +
            "and you know everyone for who they really are. However your drunken state leaves you " +
            "unable to pinpoint every role. You get to know two roles which are in play each night.";


    public void registerCallback(Player playerCallback) {
        this.playerCallback = playerCallback;

        controller = playerCallback.getController();
        players = controller.players;
    }

    public abstract String getName();

    public void handelStageStart(int stageId) {
        switch (stageId) {
            case GenericStage.STAGE_NIGHT_ONE:
            case GenericStage.STAGE_NIGHT_TWO:
                sendGenericOptions();
                break;

            default:
                //Do nothing
                break;
        }
    }

    public void sendGenericOptions() {
        try {
            JSONObject question = generateGenericOptionsMessage();
            playerCallback.getClient().sendMessage(question.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void handleMessage(int stageId, JSONObject msg, Player origin) throws JSONException {
        switch (stageId) {
            case GenericStage.STAGE_NIGHT_ONE:
            case GenericStage.STAGE_NIGHT_TWO:
                handleMessageGenericOptions(msg, origin);
                break;

            default:
                Log.d(MainActivity.TAG, "[GenericRole](handleMessage): Unable to handle message");
                break;
        }
    }

    private void handleMessageGenericOptions(JSONObject msg, Player origin) throws JSONException {
        if (msg.getInt("id") == controller.PACKET_GENERIC) {
            int answer = msg.getInt("answer");
            boolean correct = (answer == genericPhraseAnswer);

            JSONObject reply = new JSONObject();
            reply.put("id", controller.PACKET_WAIT);
            reply.put("current", 0);
            reply.put("max", 0);
            reply.put("show_progress", false);
            reply.put("correct", correct);
            reply.put("answer", genericPhraseAnswer);

            playerCallback.ready = true;
            playerCallback.getClient().sendMessage(reply.toString());
        }
    }

    private JSONObject generateGenericOptionsMessage() throws JSONException {
        int firstNumber = getRandomInt();
        int secondNumber = getRandomInt();
        int operatorIndex = getRandomInt(0, operators.length());

        String phrase = "Calculate " + firstNumber + " " + operators.charAt(operatorIndex) +
                " " + secondNumber;

        int[] options = new int[operators.length()];
        for (int i = 0; i < operators.length(); i++) {
            switch (operators.charAt(i)) {
                case '+':
                    options[i] = firstNumber + secondNumber;
                    break;
                case '-':
                    options[i] = firstNumber - secondNumber;
                    break;
                case '*':
                    options[i] = firstNumber * secondNumber;
                    break;
                case ':':
                    options[i] = firstNumber / secondNumber;
                    break;
                case '%':
                    options[i] = firstNumber % secondNumber;
                    break;
            }
            if (i == operatorIndex) genericPhraseAnswer = options[i];
        }

        JSONObject msg = new JSONObject();
        JSONArray opts = new JSONArray();
        for (int option : options) {
            opts.put(option);
        }
        msg.put("id", controller.PACKET_OPTS);
        msg.put("phrase", phrase);
        msg.put("options", opts);
        return msg;
    }

    /**
     * Gets a random integer between 2 and 12, including 2 and 12
     */
    private int getRandomInt() {
        return getRandomInt(2, 12);
    }

    private int getRandomInt(int min, int max) {
        return (int) (random.nextDouble() * max + min);
    }


    //public abstract void handleMessage(String message);
}
