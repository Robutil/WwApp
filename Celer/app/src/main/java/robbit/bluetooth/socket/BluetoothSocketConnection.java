package robbit.bluetooth.socket;

import android.bluetooth.BluetoothSocket;

public interface BluetoothSocketConnection {

    void onConnect(BluetoothSocket socket);

    void onError(Exception ex);

}
