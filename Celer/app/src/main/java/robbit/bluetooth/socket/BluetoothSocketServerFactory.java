package robbit.bluetooth.socket;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.util.UUID;

import static robbit.bluetooth.UuidFactory.sharedUuid;

public class BluetoothSocketServerFactory {

    private BluetoothServerSocket serverSocket = null;
    private boolean running = false;

    public void start(final BluetoothSocketConnection callback) {
        start(callback, true);
    }

    public void start(final BluetoothSocketConnection callback, final boolean exitOnFirstConnect) {
        stop(); //Make sure we're stopped

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
                    adapter.cancelDiscovery();

                    UUID uuid = UUID.fromString(sharedUuid);
                    serverSocket = adapter.listenUsingRfcommWithServiceRecord("ROB", uuid);

                    adapter.cancelDiscovery();
                    Thread.sleep(500);

                    running = true;
                    while (running) {
                        BluetoothSocket peer = serverSocket.accept();

                        Thread.sleep(500);
                        callback.onConnect(peer);

                        if (exitOnFirstConnect) stop();
                    }

                } catch (IOException | InterruptedException e) {
                    callback.onError(e);
                } finally {
                    stop();
                }
            }
        }).start();
    }

    public void stop() {
        running = false;
        try {
            if (serverSocket != null) {
                serverSocket.close();
                serverSocket = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
