package robbit.bluetooth.socket;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

import robbit.bluetooth.UuidFactory;

/**
 * Use this class to check for bonded devices and create a connection with them.
 */
public class BluetoothSocketClientFactory {

    /**
     * Use this method to get a list of paired devices. After choosing the correct device
     * it can be passed to BluetoothSocketClientFactory.connect()
     *
     * @return: a Set of BluetoothDevices
     */
    public Set<BluetoothDevice> getBondedDevices() {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        return adapter.getBondedDevices();
    }


    /**
     * Use this method to start a connection. The callback will be called with the onOpen()
     * method when the client is ready to be used.
     *
     * @param device:   Device to connect to
     * @param callback: Instance to be called when the connection is made
     */
    public void connect(final BluetoothDevice device, final BluetoothSocketConnection callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    UUID uuid = UUID.fromString(UuidFactory.sharedUuid);
                    BluetoothSocket socket = device.createRfcommSocketToServiceRecord(uuid);

                    socket.connect();
                    Thread.sleep(500);

                    callback.onConnect(socket);

                } catch (IOException | InterruptedException e) {
                    callback.onError(e);
                }
            }
        }).start();
    }
}
