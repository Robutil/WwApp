package robbit.bluetooth.bridge;

import robbit.net.protocol.Packet;

public interface BluetoothBridgeCallback {

    void onOpen();

    void onError(Exception ex);

    void onPacket(Packet packet);

    void onClose(Exception ex);

}
