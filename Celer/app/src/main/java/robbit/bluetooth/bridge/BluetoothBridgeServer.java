package robbit.bluetooth.bridge;

import android.bluetooth.BluetoothSocket;

import java.io.IOException;

import robbit.bluetooth.socket.BluetoothSocketConnection;
import robbit.bluetooth.socket.BluetoothSocketServerFactory;
import robbit.net.protocol.Packet;
import robbit.net.protocol.Protocol;
import robbit.net.protocol.ProtocolCallback;

public class BluetoothBridgeServer implements BluetoothSocketConnection, ProtocolCallback {

    private final BluetoothSocketServerFactory serverFactory;
    private boolean active = false;

    private BluetoothBridgeCallback callback;
    private BluetoothSocket peer;
    private Protocol protocol;

    public BluetoothBridgeServer() {
        serverFactory = new BluetoothSocketServerFactory();
    }

    public void start(BluetoothBridgeCallback callback) {
        if (active) stop();

        this.callback = callback;
        serverFactory.start(this);
    }

    @Override
    public void onConnect(BluetoothSocket socket) {
        peer = socket;
        active = true;
        try {
            protocol = new Protocol(peer.getInputStream(), peer.getOutputStream());
            protocol.startListening(this);
            callback.onOpen();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPacket(Packet packet) {
        callback.onPacket(packet);
    }

    @Override
    public void onClose(Exception ex) {
        callback.onClose(ex);
        stop();
    }

    @Override
    public void onError(Exception ex) {
        callback.onError(ex);
    }

    public boolean sendPacket(Packet packet) {
        return peer.isConnected() && protocol.sendPacket(packet);
    }

    public void stop() {
        if (!active) return;
        try {
            active = false;
            if (peer != null) peer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
