package robbit.bluetooth.bridge;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.util.Set;

import robbit.bluetooth.socket.BluetoothSocketConnection;
import robbit.bluetooth.socket.BluetoothSocketClientFactory;
import robbit.net.protocol.Packet;
import robbit.net.protocol.Protocol;
import robbit.net.protocol.ProtocolCallback;

public class BluetoothBridgeClient implements BluetoothSocketConnection, ProtocolCallback {

    private final BluetoothSocketClientFactory clientFactory;
    private final Set<BluetoothDevice> bondedDevices;

    private BluetoothSocket peer;
    private boolean active = false;

    private BluetoothBridgeCallback callback;
    private Protocol protocol;

    public BluetoothBridgeClient() {
        clientFactory = new BluetoothSocketClientFactory();
        bondedDevices = clientFactory.getBondedDevices();
    }

    public Set<BluetoothDevice> getBondedDevices() {
        return bondedDevices;
    }

    public void connect(BluetoothDevice device, BluetoothBridgeCallback callback) {
        if (active) stop();

        this.callback = callback;
        clientFactory.connect(device, this);
    }

    @Override
    public void onConnect(BluetoothSocket socket) {
        peer = socket;
        active = true;
        try {
            protocol = new Protocol(peer.getInputStream(), peer.getOutputStream());
            protocol.startListening(this);
            callback.onOpen();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPacket(Packet packet) {
        callback.onPacket(packet);
    }

    @Override
    public void onClose(Exception ex) {
        callback.onClose(ex);
        stop();
    }

    @Override
    public void onError(Exception ex) {
        callback.onError(ex);
    }

    public boolean sendPacket(Packet packet) {
        return peer.isConnected() && protocol.sendPacket(packet);
    }

    public void stop() {
        try {
            active = false;
            if (peer != null) peer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
