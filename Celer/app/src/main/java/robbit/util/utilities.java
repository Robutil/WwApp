package robbit.util;

import android.util.Log;

/**
 * 2017-05-19.
 */

public class utilities {

    private static String TAG = "ROB";
    private static boolean DEBUG = false;

    public utilities(String TAG) {
        utilities.TAG = TAG;
    }

    public static void r_log(String message) {
        r_log(DEBUG, message);
    }

    public static void r_log(boolean debug, String message) {
        if (debug) Log.d(TAG, message);
    }
}
