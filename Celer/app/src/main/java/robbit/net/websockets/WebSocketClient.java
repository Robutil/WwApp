package robbit.net.websockets;

import android.util.Log;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;

import me.robbit.robutil.celer.MainActivity;

public abstract class WebSocketClient {

    private WebSocket socket;
    private Object callback;

    public WebSocketClient(WebSocket socket) {
        this.socket = socket;
    }

    public void registerCallback(Object callback) {
        this.callback = callback;
    }

    public Object getCallback() {
        return callback;
    }

    public WebSocket getSocket() {
        return socket;
    }

    public abstract void onOpen(ClientHandshake handshake);

    public abstract void onMessage(String message);

    public abstract void onError(Exception exception);

    public abstract void onClose(int code, String reason, boolean remote);

    public void terminate() {
        socket.close();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof WebSocket) {
            return socket.equals(obj);
        } else if (obj instanceof WebSocketClient) {
            return socket.equals(((WebSocketClient) obj).getSocket());
        } else return super.equals(obj);
    }
}
