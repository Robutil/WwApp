package robbit.net.websockets;

import android.util.Log;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import me.robbit.robutil.celer.MainActivity;

public class LocalWebSockServer {

    private InetSocketAddress address;
    private WebSocketServer webSocketServer;
    private Class<WebSocketClient> webSocketClientClass;
    private Object clientCallback = null;

    public LocalWebSockServer(String hostname, int port, Class cl) throws UnknownHostException {
        this(new InetSocketAddress(hostname, port), cl);
    }

    public LocalWebSockServer(InetSocketAddress address, Class cl) throws UnknownHostException {
        this.address = address;

        //noinspection unchecked assignment
        webSocketClientClass = cl;

        initialiseWebSockServer();
    }

    public void registerWebSocketClientCallback(Object callback) {
        clientCallback = callback;
    }

    public void start() {
        Log.d(MainActivity.TAG, "[LocalWebSockServer](start): Starting WebSockServer");
        webSocketServer.start();
        webSocketServer.getWebSocketFactory();
    }

    public void stop() {
        try {
            webSocketServer.stop();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void initialiseWebSockServer() throws UnknownHostException {

        webSocketServer = new WebSocketServer(address) {

            private ArrayList<WebSocketClient> clients = new ArrayList<>();

            @Override
            public void onOpen(WebSocket conn, ClientHandshake handshake) {
                try {
                    Constructor constructor = webSocketClientClass.getDeclaredConstructor(WebSocket.class);
                    WebSocketClient newClient = (WebSocketClient) constructor.newInstance(conn);
                    clients.add(newClient);

                    if (clientCallback != null) {
                        Log.d(MainActivity.TAG, "Implementing callback");
                        newClient.registerCallback(clientCallback);
                    }

                    newClient.onOpen(handshake);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onClose(WebSocket conn, int code, String reason, boolean remote) {
                WebSocketClient client = findClientBySocket(conn);
                Log.d(MainActivity.TAG, "Reason: " + reason + " Code: " + Integer.toString(code));
                if (client != null) client.onClose(code, reason, remote);
            }

            @Override
            public void onMessage(WebSocket conn, String message) {
                WebSocketClient client = findClientBySocket(conn);
                if (client != null) {
                    client.onMessage(message);
                } else {
                    Log.d(MainActivity.TAG, "Could not find client");
                }
            }

            @Override
            public void onError(WebSocket conn, Exception ex) {
                ex.printStackTrace();

                WebSocketClient client = findClientBySocket(conn);
                if (client != null) client.onError(ex);
            }

            private WebSocketClient findClientBySocket(WebSocket conn) {
                for (WebSocketClient client : clients) {
                    if (client.equals(conn)) return client;
                }
                return null;
            }

            @Override
            public void stop() throws IOException, InterruptedException {
                super.stop();
                Log.d(MainActivity.TAG, "[LocalWebSockServer](stop): Shutting down all connections");
            }
        };
    }
}
