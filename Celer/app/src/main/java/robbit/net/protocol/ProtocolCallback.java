package robbit.net.protocol;

public interface ProtocolCallback {

    void onPacket(Packet packet);

    void onClose(Exception ex);
}
