package robbit.net.protocol;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Protocol implements Runnable {

    private InputStream input = null;
    private DataInputStream reader = null;
    private OutputStream output = null;
    private DataOutputStream writer = null;

    private ProtocolCallback callback;
    private boolean active = false;

    public Protocol(InputStream input) {
        this.input = input;
    }

    public Protocol(OutputStream output) {
        this.output = output;
    }

    public Protocol(InputStream input, OutputStream output) {
        this.input = input;
        this.output = output;
    }

    public boolean startListening(ProtocolCallback callback) {
        if (input == null) return false;

        active = true;
        this.callback = callback;
        new Thread(this).start();
        return active;
    }

    @Override
    public void run() {
        try {
            if (reader != null) reader.close();
            reader = new DataInputStream(input);

            while (active) {
                int payloadLength = reader.readInt();

                int offset = 0;
                byte[] buffer = new byte[payloadLength];
                while (offset < payloadLength) {
                    offset += reader.read(buffer, offset, payloadLength - offset);
                }

                callback.onPacket(new Packet(buffer));
            }
        } catch (IOException e) {
            callback.onClose(e);
        }
    }

    public boolean sendPacket(Packet packet) {
        if (output == null) return false;
        if (writer == null) writer = new DataOutputStream(output);

        try {

            writer.writeInt(packet.getLength());
            writer.write(packet.getContent());
            writer.flush();

        } catch (IOException e) {
            callback.onClose(e);
            return false;
        }

        return true;
    }

    public void stop() throws IOException {
        active = false;
        if (input != null) {
            if (reader != null) {
                reader.close();
                reader = null;
            }
            input.close();
            input = null;
        }

        if (output != null) {
            if (writer != null) {
                writer.close();
                writer = null;
            }
            output.close();
            output = null;
        }
    }


}
