package robbit.net.protocol;

import java.util.Arrays;

public class Packet {

    private int offset = 0;
    private byte[] payload;

    public Packet(byte[] payload) {
        this.payload = payload;
    }

    public Packet(String payload) {
        this.payload = payload.getBytes();
    }

    public int getLength() {
        return payload.length;
    }

    public byte[] getContent() {
        return payload;
    }

    @Override
    public String toString() {
        return new String(payload);
    }
}
