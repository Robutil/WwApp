package robbit.net.webserver;

import android.content.res.AssetManager;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.List;

import me.robbit.robutil.celer.MainActivity;

/**
 * Robutil 2017-05-19.
 */

public class LocalWebServer implements Runnable {

    //Debugging
    private final String TAG = "ROB";
    private boolean enableDebug = true;

    //Files to serve
    private final AssetManager assets;
    private final String defaultRoute = "index.html";

    //Cache
    HashMap<String, byte[]> cache = new HashMap<>();
    Thread cacheThread = null;

    //Server stuff
    private boolean isRunning;
    private volatile boolean preventRaceCondition = false;
    private final int port;
    private ServerSocket serverSocket = null;
    private String address = "";

    public LocalWebServer(int port, AssetManager assets) {
        this.port = port;
        this.assets = assets;
        isRunning = false;
    }

    public void start() {
        if (!preventRaceCondition && !isRunning) {
            preventRaceCondition = true;
            preCacheSomeAssets();
            new Thread(this).start();
        }
    }

    public void stop() {
        if (isRunning && serverSocket != null) {
            try {
                Log.d(MainActivity.TAG, "[LocalWebServer](stop): Stopping WebServer");
                serverSocket.close();
                serverSocket = null;

                isRunning = false;

                cacheThread.interrupt();
                Thread.currentThread().interrupt();

            } catch (IOException e) {
                Log.d(TAG, "Error closing server serverSocket: " + e);
            }
        }
    }

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(port);
            Log.d(TAG, "Local WebServer started on port: " + Integer.toString(port));
            createAddressString();

            isRunning = true;
            preventRaceCondition = false;
            while (isRunning) {
                final Socket clientSocket = serverSocket.accept();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            handleClient(clientSocket);
                        } catch (IOException e) {
                            Log.d(TAG, "Error handling client: " + e);
                        }
                    }
                }).start();

            }


        } catch (SocketException e) {
            //Stopped normally
        } catch (IOException e) {
            Log.d(TAG, "Local WebServer encountered an error: " + e);
        }
    }

    private void createAddressString() {
        try {
            String device = "wlan0";
            List<InterfaceAddress> list;
            try {
                list = NetworkInterface.getByName(device).getInterfaceAddresses();
            } catch (NullPointerException ex) {
                address = "null. No Wifi?";
                return;
            }

            for (InterfaceAddress item : list) {
                InetAddress helper = item.getAddress();

                if (helper instanceof Inet4Address) {
                    address = helper.getHostAddress();
                    break;
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    private void handleClient(Socket clientSocket) throws IOException {
        BufferedReader reader = null;
        PrintStream output = null;

        try {
            reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            output = new PrintStream(clientSocket.getOutputStream());
            String route = null;

            int failSafe = 50;
            while (isRunning) {
                route = parseRoute(reader.readLine());
                if (route != null) {
                    if (route.equals("")) {
                        route = defaultRoute;
                    }
                    break;
                }

                failSafe--;
                if (failSafe < 0) break;
            }

            byte[] content = loadContent(route);
            if (content == null) {
                writeServerError(output, "Unknown route");
                return;
            }

            sendContent(output, content, route);

        } finally {
            if (reader != null) reader.close();
            if (output != null) output.close();
        }
    }

    private String parseRoute(String input) {
        if (!TextUtils.isEmpty(input)) {
            if (input.startsWith("GET /")) {
                int charPointerRouteMessageStart = input.indexOf('/') + 1;
                int charPointerRouteMessageEnd = input.indexOf(' ', charPointerRouteMessageStart);
                return input.substring(charPointerRouteMessageStart, charPointerRouteMessageEnd);
            }
        }

        return null;
    }

    private byte[] loadContent(String route) throws IOException {
        byte[] fromCache = checkCache(route);
        if (fromCache != null) {
            return fromCache;
        }

        InputStream files = null;
        ByteArrayOutputStream data = null;

        try {
            data = new ByteArrayOutputStream();
            try {
                files = assets.open(route);
            } catch (IllegalArgumentException ex) {
                Log.d(MainActivity.TAG, "ex: " + ex.toString());
            }

            byte[] buffer = new byte[1024];
            int available, EOF = -1;
            while ((available = files.read(buffer)) != EOF) {
                data.write(buffer, 0, available);
            }
            data.flush();

            return data.toByteArray();

        } catch (NullPointerException ex) {
            return null;
        } catch (FileNotFoundException e) {
            return null;
        } finally {
            if (files != null) files.close();
            if (data != null) data.close();
        }
    }

    private byte[] checkCache(String route) {
        return cache.get(route);
    }

    private void preCacheSomeAssets() {
        cacheThread = new Thread(new Runnable() {
            @Override
            public void run() {
                String[] permanentCache = {
                        "icon/achtergrond.jpg",
                        "icon/load.gif",
                        "icon/loading.gif",
                        "icon/reload.png",
                };
                String[] temporaryCache = {
                        "card/civilian.jpg",
                        "card/wolf.jpg",
                        "js/connection.js",
                        "js/layout_manager.js",
                        "js/new_session.js"
                };

                for (String item : permanentCache) {
                    preCacheFile(item);
                }
                for (String item : temporaryCache) {
                    preCacheFile(item);
                }

                Log.d(MainActivity.TAG, "Cache init complete: " + cache.toString());

                try {
                    Thread.sleep(3 * 60 * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    cache = new HashMap<>();
                    return;
                }

                Log.d(MainActivity.TAG, "Purging some cache");

                purgeSomeCache(temporaryCache);
            }
        });
        cacheThread.start();
    }

    private void purgeSomeCache(String[] cacheFilesToRemove) {
        for (String item : cacheFilesToRemove) {
            cache.remove(item);
        }
    }

    private void preCacheFile(String route) {
        try {
            byte[] file = loadContent(route);
            if (file != null) {
                cache.put(route, file);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendContent(PrintStream output, byte[] content, String route) throws IOException {
        String header = "HTTP/1.0 200 OK";
        String type = detectExtension(route);
        String length = Integer.toString(content.length);

        output.println(header);
        output.println("Content-type: " + type);
        output.println("Content-length: " + length);
        output.println(); //Extra line after full header
        output.write(content);
        output.flush();
    }

    private String detectExtension(String route) {
        if (TextUtils.isEmpty(route)) {
            return null;
        } else if (route.endsWith(".html")) {
            return "text/html";
        } else if (route.endsWith(".js")) {
            return "application/javascript";
        } else if (route.endsWith(".css")) {
            return "text/css";
        } else {
            return "application/octet-stream";
        }
    }

    private void writeServerError(PrintStream output, String message) {
        output.println("HTTP/1.0 500 " + message);
        output.flush();
    }

    public String getAddress() {
        if (address.startsWith("null")) return address;
        else return address + ":" + Integer.toString(port) + "/";
    }

    public String getRawAddress() {
        return address;
    }

    public void doDebug(boolean val) {
        this.enableDebug = val;
    }

    public String getTAG() {
        return TAG;
    }

    public boolean isDebuggingEnabled() {
        return enableDebug;
    }

    public boolean isRunning() {
        return isRunning;
    }
}
