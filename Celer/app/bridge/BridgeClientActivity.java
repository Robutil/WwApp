package me.robbit.robutil.celer.bridge;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.Set;

import me.robbit.robutil.celer.R;
import robbit.bluetooth.bridge.BluetoothBridgeCallback;
import robbit.bluetooth.bridge.BluetoothBridgeClient;
import robbit.net.protocol.Packet;

public class BridgeClientActivity extends AppCompatActivity implements BluetoothBridgeCallback {

    private String TAG = "BRIDGE_C";
    private BluetoothBridgeClient peer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bridge_client);

        Button button = (Button) findViewById(R.id.BridgeClientActivityButton);

        peer = new BluetoothBridgeClient();
        Set<BluetoothDevice> bondedDevices = peer.getBondedDevices();

        for (BluetoothDevice device : bondedDevices) {
            if (device.getName().startsWith("HTC") || device.getName().startsWith("XT1")) {
                peer.connect(device, this);
                break;
            }
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                peer.sendPacket(new Packet("Hello\n"));
            }
        });
    }

    @Override
    public void onOpen() {
        Log.d(TAG, "New connection, sending packet");
        peer.sendPacket(new Packet("Hello "));
    }

    @Override
    public void onError(Exception ex) {
        Log.d(TAG, "Could not establish connection: " + ex.toString());
    }

    @Override
    public void onPacket(Packet packet) {
        Log.d(TAG, "Received packet: " + packet.toString());
    }

    @Override
    public void onClose(Exception ex) {
        Log.d(TAG, "Connection was closed");
    }

    @Override
    protected void onStop() {
        super.onStop();

        peer.stop();
    }
}