package me.robbit.robutil.celer.bridge;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import me.robbit.robutil.celer.R;
import robbit.bluetooth.bridge.BluetoothBridgeCallback;
import robbit.bluetooth.bridge.BluetoothBridgeServer;
import robbit.net.protocol.Packet;

public class BridgeServerActivity extends AppCompatActivity implements BluetoothBridgeCallback {

    private final String TAG = "BRIDGE_S";
    private BluetoothBridgeServer peer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bridge_server);


        Button button = (Button) findViewById(R.id.BridgeServerActivityButton);

        peer = new BluetoothBridgeServer();
        peer.start(this);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                peer.sendPacket(new Packet("World!\n"));
            }
        });
    }

    @Override
    public void onOpen() {
        Log.d(TAG, "New connection");
    }

    @Override
    public void onError(Exception ex) {
        Log.d(TAG, "Could not establish connection: " + ex.toString());
    }

    @Override
    public void onPacket(Packet packet) {
        Log.d(TAG, "Received packet: " + packet.toString());
    }

    @Override
    public void onClose(Exception ex) {
        Log.d(TAG, "Connection was closed");
    }

    @Override
    protected void onStop() {
        super.onStop();

        peer.stop();
    }

}